package fr.eseo.poo.projet.artiste;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <strong> Fonction : <code>EtreUnArtiste</code> </strong>
 * @author marindecacqueray
 * ESEO
 * A1 2020
 * Projet Etre Un Artiste 
 */
public class EtreUnArtiste {
	
	/*
	 * Main
	 */
	public static void main( String [] args) {
		@SuppressWarnings("unused")
		EtreUnArtiste jeSuisUnArtiste =  new EtreUnArtiste();
	}
	
	/*
	 * Constructeur
	 */
	public EtreUnArtiste() {
		launchBeAnartist();
	}
	
	/*
	 * Methodes
	 */
	public void launchBeAnartist() {
		PanneauDessin panneauDessin = new PanneauDessin();
		JFrame f = new JFrame("Test du panneau Barre d'outils");
		f.add(panneauDessin);
		PanneauBarreOutils panneauOutil = new PanneauBarreOutils(panneauDessin);
		f.add(panneauOutil, BorderLayout.EAST);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
