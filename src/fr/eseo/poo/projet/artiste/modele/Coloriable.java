package fr.eseo.poo.projet.artiste.modele;

import java.awt.Color;

/**
 * <strong> Fonction : <code>Coloriable</code> </strong>
 * @author marindecacqueray
 */
public interface Coloriable {
	
	/*
	 * Getters & Setters
	 */
	public abstract Color getCouleur();

	public abstract void setCouleur(Color couleur);

}
