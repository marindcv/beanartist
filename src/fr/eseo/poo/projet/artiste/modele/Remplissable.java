package fr.eseo.poo.projet.artiste.modele;

/**
 * <strong> Fonction : <code>Remplissable</code> </strong>
 * @author marindecacqueray
 */
public interface Remplissable {
	
	/*
	 * Getters & Setters
	 */
	public boolean estRempli();
	
	public void setRempli(boolean modeRemplissage);
}
