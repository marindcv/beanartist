package fr.eseo.poo.projet.artiste.modele;

import java.text.DecimalFormat;
import java.util.Locale;

/**
 * <strong> Fonction : <code>Coordonnees</code> </strong>
 * @author marindecacqueray
 */
public class Coordonnees{

	/*
	 * Accesseurs
	 */
	public static final double ABSCISSE_PAR_DEFAUT = 0;
	public static final double ORDONNEE_PAR_DEFAUT = 0;

	private double abscisse;
	private double ordonnee;

	/*
	 * Constructeurs
	 */
	public Coordonnees(double x, double y){
	    setAbscisse(x);
	    setOrdonnee(y);
	}
	public Coordonnees(){
		setAbscisse(ABSCISSE_PAR_DEFAUT);
	    setOrdonnee(ORDONNEE_PAR_DEFAUT);
    }
	
	/*
	 * Getters setters
	 */
	public double getAbscisse(){
		return this.abscisse;
	}
	
	public void setAbscisse(double x){
		this.abscisse =  x;
	}
	
	public double getOrdonnee(){
	  return this.ordonnee;
	}
	
	public void setOrdonnee(double y){
		this.ordonnee =  y;
	}
	
	/*
	 * Methodes
	 */
	public void deplacerVers(double x, double y){
		this.abscisse = x;
		this.ordonnee = y;
	}
	
	public void deplacerDe(double deltaX, double deltaY){
	    this.abscisse = this.abscisse + deltaX;
	    this.ordonnee =  this.ordonnee + deltaY;
	}
	
	public double distanceVers(Coordonnees coord){
		return Math.sqrt(Math.pow((this.abscisse-coord.abscisse),2) + 
						(Math.pow((this.ordonnee -coord.ordonnee),2)));
	}
	
	public double angleVers(Coordonnees coord){
		/*
		 * Inspired by this stackOverflow article
		 * https://stackoverflow.com/questions/9970281/java-calculating-the-angle-between-two-points-in-degrees
		 */
		if(coord.getOrdonnee() > this.getOrdonnee()) {
			return -Math.atan2(	this.getOrdonnee() - coord.getOrdonnee(), 
								coord.getAbscisse() - this.getAbscisse());
		}
		else {
			return Math.atan2(	coord.getOrdonnee() - this.getOrdonnee(), 
								coord.getAbscisse() - this.getAbscisse());
		}
	}
	  
	public String toString() {
		Locale currentLocale = Locale.getDefault();
		DecimalFormat current = (DecimalFormat)DecimalFormat.getNumberInstance(currentLocale);
		current.setMinimumFractionDigits(1);
		current.setMaximumFractionDigits(2);
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		sb.append(current.format(this.getAbscisse()));
		sb.append(" , ");
		sb.append(current.format(this.getOrdonnee()));
		sb.append(")");
		return sb.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(abscisse);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(ordonnee);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordonnees other = (Coordonnees) obj;
		if (Double.doubleToLongBits(abscisse) != Double.doubleToLongBits(other.abscisse))
			return false;
		if (Double.doubleToLongBits(ordonnee) != Double.doubleToLongBits(other.ordonnee))
			return false;
		return true;
	}
}
