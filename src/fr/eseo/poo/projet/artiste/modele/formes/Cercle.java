package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;

/**
 * <strong> Fonction : <code>Cercle</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.modele.formes.Ellipse
 */
public class Cercle extends Ellipse {
	
	/*
	 * Accesseurs
	 */
	private static final String ARGUMENT_EXCEPTION = "Width connot be negative";
	
	/*
	 * Constructeurs
	 */
	public Cercle() {
		super();
	}
	
	public Cercle(double diametre) {
		super(diametre, diametre);
		if(diametre < 0) {
			throw new IllegalArgumentException(ARGUMENT_EXCEPTION);
		}
	}
	
	public Cercle(Coordonnees position) {
		super(position);
	}
	
	public Cercle(Coordonnees position, double diametre) {
		super(position, diametre, diametre);
		if(diametre < 0) {
			throw new IllegalArgumentException(ARGUMENT_EXCEPTION);
		}
	}
	
	/*
	 * Getters &Setters
	 */
	@Override
	public void setHauteur(double diametre) {
		super.setHauteur(diametre);
		super.setLargeur(diametre);
		if(diametre < 0) {
			throw new IllegalArgumentException("Height connot be negative");
		}
	}
	
	@Override
	public void setLargeur(double diametre) {
		super.setLargeur(diametre);
		super.setHauteur(diametre);
		if(diametre < 0) {
			throw new IllegalArgumentException(ARGUMENT_EXCEPTION);
		}
	}
	
	/*
	 * Methodes
	 */
	@Override
	public double perimetre() {
		return 2*Math.PI*this.getLargeur()/2;
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	
}
