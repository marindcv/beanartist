package fr.eseo.poo.projet.artiste.modele.formes;

import java.text.DecimalFormat;
import java.util.Locale;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;

/**
 * <strong> Fonction : <code>Ligne</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.modele.formes.Forme
 */
public class Ligne extends Forme {
	
	/*
	 * Accesseurs
	 */
	public static final double EPSILON = 0.5;
	
	/*
	 * Constructor
	 */
	public Ligne() {
		super();
	}
	
	public Ligne(double largeur, double hauteur) {
		super(largeur, hauteur);
	}
	
	public Ligne(Coordonnees coordonnees) {
		super(coordonnees);
	}
	
	public Ligne(Coordonnees coordonnees, double largeur, double hauteur) {
		super(coordonnees, largeur, hauteur);
	}
	
	/*
	 * Getters Setters
	 */
	public Coordonnees getC1() {
		return this.getPosition();
	}
	
	public void setC1(Coordonnees coordonnees) {
		Coordonnees c2 = this.getC2();
		this.setPosition(coordonnees);

		if(this.getC2().getAbscisse()>coordonnees.getAbscisse()) {
			this.setLargeur(c2.getAbscisse()-coordonnees.getAbscisse());
		}
		else {
			this.setLargeur(-(coordonnees.getAbscisse() - c2.getAbscisse()));
		}
		if(coordonnees.getOrdonnee()>this.getC1().getOrdonnee()) {
			this.setHauteur(c2.getOrdonnee()-coordonnees.getOrdonnee());
		}
		else {
			this.setHauteur(-(coordonnees.getOrdonnee()-c2.getOrdonnee()));
		}	
	}
	
	public Coordonnees getC2() {
		double x = this.getC1().getAbscisse()+this.getLargeur();
		double y = this.getC1().getOrdonnee()+this.getHauteur(); 
		return new Coordonnees(x,y);
	}
	
	public void setC2(Coordonnees coordonnees) {
		
		if(coordonnees.getAbscisse()>this.getC1().getAbscisse()) {
			this.setLargeur((coordonnees.getAbscisse() - this.getC1().getAbscisse()));
		}
		else {
			this.setLargeur( -(this.getC1().getAbscisse() - coordonnees.getAbscisse()));
		}
		if(coordonnees.getOrdonnee()>this.getC1().getOrdonnee()) {
			this.setHauteur( (coordonnees.getOrdonnee() - this.getC1().getOrdonnee()));
		}
		else {
			this.setHauteur( -(this.getC1().getOrdonnee() - coordonnees.getOrdonnee()));
		}
	}
	
	public double getAngleVers() {
		if(this.getC2().getOrdonnee() < this.getC1().getOrdonnee()) {
			
			return Math.toDegrees(this.getPosition().angleVers(getC2())+2*Math.PI);
		}
		else {
			return Math.toDegrees(Math.abs(this.getPosition().angleVers(getC2())));
		}
	}
	
	@Override
	public String toString() {
		
		Locale currentLocale = Locale.getDefault();
		DecimalFormat current = (DecimalFormat)DecimalFormat.getNumberInstance(currentLocale);
		StringBuilder sb = new StringBuilder();
		current.setMinimumFractionDigits(1);
		current.setMaximumFractionDigits(2);
		
		sb.append("[");
		sb.append(getClass().getSimpleName());
		sb.append("] c1 : (");
		sb.append(current.format(this.getC1().getAbscisse()));
		sb.append(" , ");
		sb.append(current.format(this.getC1().getOrdonnee()));
		sb.append(") c2 : (");
		sb.append(current.format(this.getC2().getAbscisse()));
		sb.append(" , ");
		sb.append(current.format(this.getC2().getOrdonnee()));
		sb.append(") longueur : ");
		sb.append(current.format(this.getC1().distanceVers(getC2())));
		sb.append(" angle : ");
		sb.append(current.format(getAngleVers()));
		sb.append("°");
		sb.append(" couleur = ");
		sb.append("R");
		sb.append(getCouleur().getRed());
		if(!currentLocale.equals(Locale.FRENCH)) {
			sb.append(",G");
		}
		else {
			sb.append(",V");
		}
		sb.append(getCouleur().getGreen());
		sb.append(",B");
		sb.append(getCouleur().getBlue());
		
		return sb.toString();
	}
	
	@Override
	public double aire() {
		return 0;
	}
	
	@Override
	public double perimetre() {
		return this.getC1().distanceVers(getC2());
	}

	@Override
	public boolean contient(Coordonnees coordonnees) {
		return 	Math.abs(this.getC1().distanceVers(coordonnees))+
				Math.abs(coordonnees.distanceVers(getC2()))-
				Math.abs(this.getC1().distanceVers(this.getC2()))< EPSILON;
	}
}
