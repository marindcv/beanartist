package fr.eseo.poo.projet.artiste.modele.formes;

import java.text.DecimalFormat;
import java.util.Locale;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.Remplissable;

/**
 * <strong> Fonction : <code>Ellipse</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.modele.formes.Forme
 * @see fr.eseo.poo.projet.artiste.modele.Remplissable
 */
public class Ellipse extends Forme implements Remplissable {
	
	/*
	 * Accesseurs
	 */
	private Boolean rempli;
	
	/*
	 * Constructeur
	 */
	public Ellipse() {
		super();
		setRempli(false);
	}
	
	public Ellipse(double largeur, double hauteur) {
		super(largeur, hauteur);
		if(largeur < 0 || hauteur < 0) {
			throw new IllegalArgumentException(" Width or Hight can't be negative");
		}
		setRempli(false);
	}
	
	public Ellipse(Coordonnees coordonnees){
		super(coordonnees);
		setRempli(false);
	}
	
	public Ellipse(Coordonnees coordonnees, double largeur, double hauteur) {
		super(coordonnees, largeur, hauteur);
		if(largeur < 0 || hauteur < 0) {
			throw new IllegalArgumentException(" Width or Hight can't be negative");
		}
		setRempli(false);
	}
	
	/*
	 * Getters Setters
	 */
	@Override
	public void setHauteur(double hauteur) {
		super.setHauteur(Math.abs(hauteur));
		if(hauteur < 0) {
			throw new IllegalArgumentException(" Hight can't be negative");
		}
	}
	
	@Override
	public void setLargeur(double largeur) {
		super.setLargeur(Math.abs(largeur));
		if(largeur < 0) {
			throw new IllegalArgumentException(" Width can't be negative");
		}
	}
	
	/*
	 * Methode
	 */
	@Override
	public String toString() {
		/**
		 * Code fourni par Mme. Claire Belliard à cause d'un problème de validation dans l'assignement center
		 */

		Locale currentLocale = Locale.getDefault();
		DecimalFormat current = (DecimalFormat)DecimalFormat.getNumberInstance(currentLocale);
		current.setGroupingUsed(false);
		current.setMinimumFractionDigits(1);
		current.setMaximumFractionDigits(2);

		StringBuilder sb = new StringBuilder();

		sb.append("[");
		sb.append(getClass().getSimpleName());
		if(estRempli()) {
			sb.append("-Rempli");
		}
		sb.append("] : pos (");
		sb.append(current.format(this.getPosition().getAbscisse()));
		sb.append(" , ");
		sb.append(current.format(this.getPosition().getOrdonnee()));
		sb.append(") dim ");
		sb.append(current.format(getLargeur()));
		sb.append(" x ");
		sb.append(current.format(getHauteur()));
		sb.append(" périmètre : ");
		sb.append(current.format(perimetre()));
		sb.append(" aire : ");
		sb.append(current.format(aire()));
		sb.append(" couleur = ");
		sb.append("R");
		sb.append(getCouleur().getRed());
		if(!currentLocale.equals(Locale.FRENCH)) {
			sb.append(",G");
		}
		else {
			sb.append(",V");
		}
		sb.append(getCouleur().getGreen());
		sb.append(",B");
		sb.append(getCouleur().getBlue());

		return sb.toString(); 
	}

	@Override
	public double aire() {
		double a = Math.min(this.getLargeur(), this.getHauteur())/2;
		double b = Math.max(this.getLargeur(), this.getHauteur())/2;
		return Math.PI*a*b;
	}

	@Override
	public double perimetre() {
		double a = Math.min(this.getLargeur(), this.getHauteur())/2;
		double b = Math.max(this.getLargeur(), this.getHauteur())/2;
		double h = Math.pow((a-b)/(a+b),2);
		return Math.PI*(a+b)*(1+(3*h /(10+Math.sqrt(4-3*h))));
	}
	
	@Override
	public boolean contient(Coordonnees coordonnees) {
		/**
		 * Made with the help of this StackExchange topic : 
		 * https://math.stackexchange.com/questions/76457/check-if-a-point-is-within-an-ellipse
		 */
		double xMinusHSquareDividedByRSquareX = Math.pow((coordonnees.getAbscisse()
				-(this.getLargeur()/2+this.getPosition().getAbscisse())),2)/Math.pow(this.getLargeur()/2, 2);
		
		double yMinusKSquareDividedByRSquareY = Math.pow((coordonnees.getOrdonnee()
				-(this.getHauteur()/2+this.getPosition().getOrdonnee())),2)/Math.pow(this.getHauteur()/2, 2);
		
		return xMinusHSquareDividedByRSquareX + yMinusKSquareDividedByRSquareY <= 1;
	}
	
	@Override
	public boolean estRempli() {
		return this.rempli;
	}

	@Override
	public void setRempli(boolean rempli) {
		this.rempli = rempli;
	}
}
