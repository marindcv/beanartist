package fr.eseo.poo.projet.artiste.modele.formes;

import java.awt.Color;

import javax.swing.UIManager;

import fr.eseo.poo.projet.artiste.modele.Coloriable;
import fr.eseo.poo.projet.artiste.modele.Coordonnees;

/**
 * <strong> Fonction : <code>Forme</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.modele.Coordonnees
 * @see fr.eseo.poo.projet.artiste.modele.Coloriable
 */
public abstract class Forme implements Coloriable {
	
	/*
	 * Accesseurs
	 */
	public static final double LARGEUR_PAR_DEFAUT = 100.0;
	public static final double HAUTEUR_PAR_DEFAUT = 100.0;
	public static final Color COULEUR_PAR_DEFAUT = UIManager.getColor("Panel.foreground");
	private Coordonnees position;
	
	private double largeur;
	private double hauteur;
	public Color couleur;
	
	/*
	 * Constructeurs
	 */
	public Forme() {
		this(new Coordonnees(), LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT);
	}
	public Forme(double largeur, double hauteur) {
		this(new Coordonnees(), largeur, hauteur);
	}
	public Forme(Coordonnees position) {
		this(position, LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT);
	}
	public Forme(Coordonnees position, double largeur, double hauteur) {
		this.setPosition(position);
		this.setLargeur(largeur);
		this.setHauteur(hauteur);
		this.setCouleur(COULEUR_PAR_DEFAUT);
		
	}
	
	/*
	 * Getters & Setters
	 */
	public Coordonnees getPosition() {
		return this.position;
	}
	public void setPosition(Coordonnees position){ 
		this.position=position;
	}
	
	public void setLargeur(double largeur) {
		this.largeur = largeur;
	}
	public void setHauteur(double hauteur) {
		this.hauteur = hauteur;
	}
	public double getLargeur() {
		return this.largeur;
	}
	public double getHauteur() {
		return this.hauteur;
	}
	
	public double getCadreMaxX() {
        return (Math.max(getPosition().getAbscisse() + getLargeur(),  getPosition().getAbscisse()));
    }
    public double getCadreMaxY() {
        return (Math.max(getPosition().getOrdonnee() +  getHauteur(), getPosition().getOrdonnee()));
    }
    public double getCadreMinX() {
        return (Math.min(getPosition().getAbscisse() + getLargeur(),  getPosition().getAbscisse()));
    }
    public double getCadreMinY() {
        return (Math.min(getPosition().getOrdonnee() +  getHauteur(), getPosition().getOrdonnee()));
    }

    public Color getCouleur() {
		return this.couleur;
	}

	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}
    
    /*
     * Methodes
     */
	public void deplacerVers(double x, double y){
        getPosition().deplacerVers(x,y);
    }
   
    public void deplacerDe(double deltaX, double deltaY){
        getPosition().deplacerDe(deltaX,deltaY);
    }
    
    public abstract double aire ();
    
	public abstract double perimetre ();
	
	public abstract boolean contient(Coordonnees coordonnees);
	
}
