package fr.eseo.poo.projet.artiste.modele.formes;

import java.awt.Polygon;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.Remplissable;

/**
 * <strong> Fonction : <code>Etoile</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.modele.formes.Forme
 * @see fr.eseo.poo.projet.artiste.modele.Remplissable
 */
public class Etoile extends Forme implements Remplissable {

	/*
	 * Accesseurs
	 */
	public static final int NOMBRE_BRANCHES_PAR_DEFAUT = 5;
	public static final double ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT = Math.PI/2;
	public static final double LONGUEUR_BRANCHE_PAR_DEFAUT = 0.5;

	private List<Coordonnees> coordonnees;
	private int nbBranche;
	private double anglePremiereBranche;
	private double longueurBranche;
	private boolean rempli;

	/*
	 * Constructeurs
	 */
	public Etoile() {
		this(	new Coordonnees(), Forme.LARGEUR_PAR_DEFAUT, NOMBRE_BRANCHES_PAR_DEFAUT, 
				ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT, LONGUEUR_BRANCHE_PAR_DEFAUT);
	}

	public Etoile(Coordonnees coordonnees) {
		this(	coordonnees, Forme.LARGEUR_PAR_DEFAUT, NOMBRE_BRANCHES_PAR_DEFAUT, 
				ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT, LONGUEUR_BRANCHE_PAR_DEFAUT);
	}

	public Etoile(double taille) {
		this(	new Coordonnees(), taille, NOMBRE_BRANCHES_PAR_DEFAUT, ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT,
				LONGUEUR_BRANCHE_PAR_DEFAUT);
	}

	public Etoile(Coordonnees coordonnees, double taille) {
		this(	coordonnees, taille, NOMBRE_BRANCHES_PAR_DEFAUT, ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT,
				LONGUEUR_BRANCHE_PAR_DEFAUT);
	}

	public Etoile(Coordonnees coordonnees, double taille, int nombreBranches, double anglePremiereBranche,
			double longueurBranche) {
		
		super(coordonnees);
		this.coordonnees = new ArrayList<Coordonnees>();
		this.setNombreBranches(nombreBranches);
		this.setAnglePremiereBranche(anglePremiereBranche);
		this.setLongueurBranche(longueurBranche);
		this.setRempli(false);
		this.resetCoordonneeBranches();
		this.setHauteur(taille);
	}

	/*
	 * Getters & Setters
	 */
	public int getNombreBranches() {
		return nbBranche;
	}

	public void setNombreBranches(int nbBranches) {
		if (nbBranches >= 3 && nbBranches <= 15 && nbBranches != 0) {
			this.nbBranche = nbBranches;
			this.deplacerCoordonneesBranche(this.getHauteur());
		} else {
			if (nbBranches < 3) {
				throw new IllegalArgumentException("Stars need at least 3 branches");
			} else if (nbBranches > 15) {
				throw new IllegalArgumentException("Stars need to have 15 branches or less");
			}
		}
	}

	public double getAnglePremiereBranche() {
		return anglePremiereBranche;
	}

	public void setAnglePremiereBranche(double anglePremiereBranche) {
		if (anglePremiereBranche > - Math.PI && anglePremiereBranche <=  Math.PI) {
			this.anglePremiereBranche = anglePremiereBranche;
			this.deplacerCoordonneesBranche(this.getHauteur());
		} else {
			if (anglePremiereBranche <= - Math.PI) {
				throw new IllegalArgumentException("Stars' angle between branch need to be more than -Pi rad");
			} else if (anglePremiereBranche >  Math.PI) {
				throw new IllegalArgumentException("Stars' angle between branch need to be equal or less than Pi rad");
			}
		}
	}

	public double getLongueurBranche() {
		return longueurBranche;
	}

	public void setLongueurBranche(double longueurBranche) {
		if (longueurBranche >= 0 && longueurBranche <= 1) {
			this.longueurBranche = longueurBranche;
			this.deplacerCoordonneesBranche(this.getHauteur());
		} else {
			if (longueurBranche < 0) {
				throw new IllegalArgumentException("Stars' branch length need to be equal or grater than 0%");
			} else if (longueurBranche > 1) {
				throw new IllegalArgumentException("Stars' branch length need to be equal or less than 100%");
			}
		}
	}
	
	@Override
	public void setHauteur(double hauteur) {
		if(hauteur > 0) {
			super.setHauteur(Math.abs(hauteur));
			super.setLargeur(Math.abs(hauteur));
			if(this.getCoordonnees() != null) {
				this.deplacerCoordonneesBranche(hauteur);
				System.err.println("Branch Coordonnate : "+this.getCoordonnees());
			}
		}
		else {
			throw new IllegalArgumentException("Diameter connot be negative nor null");
		}
	}

	@Override
	public void setLargeur(double largeur) {
		this.setHauteur(largeur);
	}

	public List<Coordonnees> getCoordonnees() {
		return this.coordonnees;
	}
	
	private void resetCoordonneeBranches() {	
		this.getCoordonnees().clear();	
	}
	
	public Coordonnees getEtoileCentre(){
		return new Coordonnees(	getPosition().getAbscisse()+getHauteur()/2, 
								getPosition().getOrdonnee()+getHauteur()/2);
	}
	
	@Override
	public boolean estRempli() {
		return this.rempli;
	}

	@Override
	public void setRempli(boolean modeRemplissage) {
		this.rempli = modeRemplissage;
	}
	
	/*
	 * Mathodes
	 */
	private void deplacerCoordonneesBranche(double hauteur) {
		double angleToNextBranch = (Math.PI * 2)/this.getNombreBranches();
		double actualAngle = this.getAnglePremiereBranche();
		
		this.resetCoordonneeBranches();
		
		/*
		 * On place toute les coordonnees au centre de l'étoile
		 */
		for (int i = 0; i < this.getNombreBranches()*2; i++) {
			this.getCoordonnees().add(this.getEtoileCentre());
		}
		
		for (Coordonnees coordonneesBranche : this.getCoordonnees()) {
			
			/*
			 * Déplace le point en fonction de la hauteur et de l'angle
			 * Le Point de base est initialisé au millieu de l'étoile (setCoordonneesBranche)
			 * Selon s'il s'agit d'un sommet ou d'un creux on va utiliser des methodes un peu differente
			 */
			
			/*
			 * S'il sagit d'un sommet:
			 * On déplace le point de la moitier le la hauteur multiplier par le cosinus de l'angle
			 */
			if(this.getCoordonnees().indexOf(coordonneesBranche)%2 == 0) {
				coordonneesBranche.deplacerDe(	hauteur/2 * Math.cos(actualAngle),
												hauteur/2 * Math.sin(actualAngle));
			}
			/*
			 * Sinon le point est egal à au centre plus la moitier de la hauteur multiplier par 
			 * 1 moins la longueur de la branche multipier par le cosinus (ou sinus selon 
			 * s'il s'agit de X ou Y) de la difference de l'angle avec l'angle suivant
			 */
			else {
				coordonneesBranche.deplacerDe(
												hauteur/2 * (1 - this.getLongueurBranche())*
												Math.cos(actualAngle + angleToNextBranch/2), 
												hauteur/2 * (1 - this.getLongueurBranche())*
												Math.sin(actualAngle + angleToNextBranch/2));
				/*
				 * ici on met à jour l'angle (Uniquement apres avoir passé le sommet ET le creux) 
				 * ce qui m'as pris un moment à trouvé :)
				 */
				actualAngle = actualAngle + angleToNextBranch;
			}	
		}
	}

	@Override
	public double aire() {
		/*
		 * Calcul de l'aire d'un des triangle (branche)
		 */
		double base = this.getCoordonnees().get(1).distanceVers(this.getCoordonnees().get(3));
		double cote = this.getCoordonnees().get(1).distanceVers(this.getCoordonnees().get(2));
		double h = Math.sqrt(Math.pow(cote, 2)-Math.pow(0.5*base, 2));
		double triangle =  (base * h) /2;
		
		double airAllTriangle = triangle * this.getNombreBranches();
			
		/*
		 * Calcul du polygone regulier
		 * on le considère égal à l'aire de NombreBranche*2 triangles rectangle : 
		 * https://fr.wikihow.com/trouver-l%27aire-d%27un-polygone-régulier
		 * Soit p le perimetre du polygone
		 */
		double p = base * this.getNombreBranches();
		/*
		 * a la distance entre le centre du polygone et le millieu d'un coté
		 */
		double a = base/(2*Math.tan(Math.PI/this.getNombreBranches()));
		double airePolygone = (p*a)/2;
				
		return airePolygone + airAllTriangle;
	}

	@Override
	public double perimetre() {
		return 	this.getCoordonnees().get(0).distanceVers(this.getCoordonnees().get(1)) * 
				(this.getNombreBranches()*2);
	}

	@Override
	public boolean contient(Coordonnees coordonate) {
		
		List <Integer> allCoordonneesX = new ArrayList<Integer>();
		List <Integer> allCoordonneesY = new ArrayList<Integer>();
		
		for(Coordonnees currentEtoile : this.getCoordonnees()) {
			 allCoordonneesX.add((int) Math.round(currentEtoile.getAbscisse()));
			 allCoordonneesY.add((int) Math.round(currentEtoile.getOrdonnee()));
		}
		
		Polygon monEtoile = new Polygon(toIntArray(allCoordonneesX), toIntArray(allCoordonneesY), 
							this.getNombreBranches()*2);
		
		return monEtoile.contains(coordonate.getAbscisse(),coordonate.getOrdonnee());
	}	
	
	public String toString() {

		Locale currentLocale = Locale.getDefault();
		DecimalFormat current = (DecimalFormat) DecimalFormat.getNumberInstance(currentLocale);
		current.setGroupingUsed(false);
		current.setMinimumFractionDigits(1);
		current.setMaximumFractionDigits(2);

		StringBuilder sb = new StringBuilder();

		sb.append("[");
		sb.append(this.getClass().getSimpleName());
		if (estRempli()) {
			sb.append("-Rempli");
		}
		sb.append("] : ");
		sb.append("pos (");
		sb.append(current.format(this.getPosition().getAbscisse()));
		sb.append(" , ");
		sb.append(current.format(this.getPosition().getOrdonnee()));
		sb.append(") dim ");
		sb.append(current.format(getLargeur()));
		sb.append(" x ");
		sb.append(current.format(getHauteur()));
		sb.append(" périmètre : ");
		sb.append(current.format(perimetre()));
		sb.append(" aire : ");
		sb.append(current.format(aire()));
		sb.append(" couleur = ");
		sb.append("R");
		sb.append(getCouleur().getRed());
		if (!currentLocale.equals(Locale.FRENCH)) {
			sb.append(",G");
		} else {
			sb.append(",V");
		}
		sb.append(getCouleur().getGreen());
		sb.append(",B");
		sb.append(getCouleur().getBlue());

		return sb.toString();
	}
	
	/*
	 * Code find on https://stackoverflow.com/questions/960431/how-to-convert-listinteger-to-int-in-java 
	 * Y'a probablement mieux ,mais ça fera l'affaire pour l'instant
	 * Transforme une array en int[]
	 */
	private int[] toIntArray(List<Integer> list){
	  int[] ret = new int[list.size()];
	  for(int i = 0;i < ret.length;i++)
	    ret[i] = list.get(i);
	  return ret;
	}
}
