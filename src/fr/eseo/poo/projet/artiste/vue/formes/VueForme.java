package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.Graphics2D;

import fr.eseo.poo.projet.artiste.modele.formes.Forme;

/**
 * <strong> Fonction : <code>VueForme</code> </strong>
 * @author marindecacqueray
 */
public abstract class VueForme{
	
	/*
	 * Accesseurs
	 */
	protected final Forme forme;
	
	/*
	 * Constructeurs
	 */
	public  VueForme(Forme forme) {
		this.forme = forme;
	}
	
	/*
	 * Getters & Setters
	 */
	public Forme getForme() {
		return forme;
	}
	
	/*
	 * Methodes
	 */
	public abstract void affiche(Graphics2D g2d);
}
