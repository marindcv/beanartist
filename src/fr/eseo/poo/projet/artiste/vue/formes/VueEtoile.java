package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;

/**
 * <strong> Fonction : <code>VueEtoile</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.vue.formes.VueForme
 */
public class VueEtoile extends VueForme {

	/*
	 * Constructeurs
	 */
	public VueEtoile(Etoile etoile) {
		super(etoile);
	}

	/*
	 * Methodes
	 */
	@Override
	public void affiche(Graphics2D g2d) {
		Color couleur = g2d.getColor();
		g2d.setColor(this.getForme().getCouleur());
		
		Etoile etoile = (Etoile) this.getForme();
		
		List <Integer> allCoordonneesX = new ArrayList<Integer>();
		List <Integer> allCoordonneesY = new ArrayList<Integer>();
		
		for(Coordonnees currentEtoile : etoile.getCoordonnees()) {
			 allCoordonneesX.add((int) Math.round(currentEtoile.getAbscisse()));
			 allCoordonneesY.add((int) Math.round(currentEtoile.getOrdonnee()));
		}
		
		if(((Etoile) this.getForme()).estRempli()) {
			g2d.fillPolygon(toIntArray(allCoordonneesX), toIntArray(allCoordonneesY), etoile.getNombreBranches()*2);
		}
		System.err.println("Coordonnee branche X : "+ allCoordonneesX +"Coordonnee branche Y : "+allCoordonneesY);
		g2d.drawPolygon(toIntArray(allCoordonneesX), toIntArray(allCoordonneesY), etoile.getNombreBranches()*2);
		g2d.setColor(couleur);
	}
	
	/*
	 * Code find on https://stackoverflow.com/questions/960431/how-to-convert-listinteger-to-int-in-java 
	 * Y'a probablement mieux ,mais ça fera l'affaire pour l'instant
	 */
	int[] toIntArray(List<Integer> list){
		  int[] ret = new int[list.size()];
		  for(int i = 0;i < ret.length;i++)
		    ret[i] = list.get(i);
		  return ret;
		}
}
