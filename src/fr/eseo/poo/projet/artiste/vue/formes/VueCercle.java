package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.formes.Cercle;

/**
 * <strong> Fonction : <code>VueCercle</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.vue.formes.Ellipse
 */
public class VueCercle extends VueEllipse{
	
	/*
	 * Constructeurs
	 */
	public VueCercle(Cercle cercle) {
		super(cercle);
	}
}
