package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.Color;
import java.awt.Graphics2D;

import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;

/**
 * <strong> Fonction : <code>VueEllipse</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.vue.formes.VueForme
 */
public class VueEllipse extends VueForme {
	
	/*
	 * Constructeurs
	 */
	public VueEllipse(Ellipse ellipse) {
		super(ellipse);
	}

	/*
	 * Methodes
	 */
	@Override
	public void affiche(Graphics2D g2d) {
		Color couleur = g2d.getColor();
		g2d.setColor(this.getForme().getCouleur());
		if(((Ellipse) this.getForme()).estRempli()) {
			g2d.fillOval((int) Math.round(this.getForme().getPosition().getAbscisse()), 
					(int) Math.round(this.getForme().getPosition().getOrdonnee()), 
					(int) Math.round(this.getForme().getLargeur()), 
					(int) Math.round(this.getForme().getHauteur()));
		}
		g2d.drawOval((int) Math.round(this.getForme().getPosition().getAbscisse()), 
				(int) Math.round(this.getForme().getPosition().getOrdonnee()), 
				(int) Math.round(this.getForme().getLargeur()), 
				(int) Math.round(this.getForme().getHauteur()));
		g2d.setColor(couleur);
	}
}
