package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.Color;
import java.awt.Graphics2D;

import fr.eseo.poo.projet.artiste.modele.formes.Ligne;

/**
 * <strong> Fonction : <code>VueLigne</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.vue.formes.VueForme
 */
public class VueLigne extends VueForme {
	
	/*
	 * Constructeurs
	 */
	public VueLigne(Ligne ligne) {
		super(ligne);
	}
	
	/*
	 * Methodes
	 */
	@Override
	public void affiche(Graphics2D g2d) {
		Color couleur = g2d.getColor();
		g2d.setColor(this.getForme().getCouleur());
		g2d.drawLine((int) Math.round(this.getForme().getPosition().getAbscisse()), 
				(int) Math.round(this.getForme().getPosition().getOrdonnee()), 
				(int) Math.round(this.getForme().getPosition().getAbscisse() + this.getForme().getLargeur()), 
				(int) Math.round(this.getForme().getPosition().getOrdonnee() + this.getForme().getHauteur()));
		g2d.setColor(couleur);
	}
	
}
