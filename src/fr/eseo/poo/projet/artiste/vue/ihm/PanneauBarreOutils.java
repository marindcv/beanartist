package fr.eseo.poo.projet.artiste.vue.ihm;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;

import fr.eseo.poo.projet.artiste.controleur.actions.ActionChoisirCouleur;
import fr.eseo.poo.projet.artiste.controleur.actions.ActionChoisirForme;
import fr.eseo.poo.projet.artiste.controleur.actions.ActionChoisirRemplissage;
import fr.eseo.poo.projet.artiste.controleur.actions.ActionEffacer;
import fr.eseo.poo.projet.artiste.controleur.actions.ActionSelectionner;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;

/**
 * <strong> Fonction : <code>PanneauBarreOutils</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.vue.PanneauDessin
 */
@SuppressWarnings("serial")
public class PanneauBarreOutils extends javax.swing.JPanel {
	
	/*
	 * Accesseurs
	 */
	private PanneauDessin panneauDessin;
	private ButtonGroup group = new ButtonGroup();
	public static final String BRANCHE_SPINNER_NOM = "Nombre de branches";
	public static final String LONGUEUR_SPINNER_NOM = "Longeur des branches";
	JSpinner nombrebranche;
	JSpinner longeurBranche;
	
	/*
	 * Constructeurs
	 */
	public PanneauBarreOutils(PanneauDessin panneauDessin) {
		this.setBackground(new Color(40,40,42));
		this.panneauDessin = panneauDessin;
		BoxLayout myBox = new BoxLayout(this, BoxLayout.PAGE_AXIS);
		this.setLayout(myBox);
		initComponents();
	}
	
	public void initComponents() {
		JButton button = new JButton( new ActionEffacer(this.panneauDessin));
		button.setName(ActionEffacer.NOM_ACTION);
		this.add(button);
		
		JToggleButton buttonLigne = new JToggleButton(new ActionChoisirForme(this.panneauDessin, this, ActionChoisirForme.NOM_ACTION_LIGNE));
		/*
		 * La ligne ci-dessous est inutile mais obligatoire pour l'ACSM
		 */
		buttonLigne.setName(ActionChoisirForme.NOM_ACTION_LIGNE);
		group.add(buttonLigne);
		
		JToggleButton buttonEllipse=new JToggleButton(new ActionChoisirForme(this.panneauDessin, this,ActionChoisirForme.NOM_ACTION_ELLIPSE));
		/*
		 * La ligne ci-dessous est inutile mais obligatoire pour l'ACSM
		 */
		buttonEllipse.setName(ActionChoisirForme.NOM_ACTION_ELLIPSE);
		group.add(buttonEllipse);
		
		JToggleButton buttonCercle=new JToggleButton(new ActionChoisirForme(this.panneauDessin, this, ActionChoisirForme.NOM_ACTION_CERCLE));
		/*
		 * La ligne ci-dessous est inutile mais obligatoire pour l'ACSM
		 */
		buttonCercle.setName(ActionChoisirForme.NOM_ACTION_CERCLE);
		group.add(buttonCercle);
		
		JLabel nbBranche = new JLabel(BRANCHE_SPINNER_NOM);
		this.nombrebranche = new JSpinner(new SpinnerNumberModel(Etoile.NOMBRE_BRANCHES_PAR_DEFAUT,3,15,1));
		this.nombrebranche.setName(BRANCHE_SPINNER_NOM);
		this.nombrebranche.setMaximumSize(new Dimension(50,30));

		
		JLabel lgBranche = new JLabel(LONGUEUR_SPINNER_NOM); 
		this.longeurBranche =  new JSpinner(new SpinnerNumberModel(Etoile.LONGUEUR_BRANCHE_PAR_DEFAUT, 0,1,0.01));
		this.longeurBranche.setName(LONGUEUR_SPINNER_NOM);
		this.longeurBranche.setMaximumSize(new Dimension(60,30));
		
		JToggleButton buttonEtoile=new JToggleButton(new ActionChoisirForme(this.panneauDessin, this, ActionChoisirForme.NOM_ACTION_ETOILE));
		buttonEtoile.setName(ActionChoisirForme.NOM_ACTION_ETOILE);
		group.add(buttonEtoile);	
		
		JToggleButton buttonSelection = new JToggleButton( new ActionSelectionner(this.panneauDessin));
		buttonSelection.setName(ActionSelectionner.NOM_ACTION);
		group.add(buttonSelection);
		
		this.add(buttonLigne);
		this.add(buttonEllipse);
		this.add(buttonCercle);
		this.add(buttonEtoile);
		this.add(nbBranche);
		this.add(nombrebranche);
		this.add(lgBranche);
		this.add(longeurBranche);
		this.add(buttonSelection);
		
		JButton buttonColor = new JButton( new ActionChoisirCouleur(panneauDessin));
		buttonColor.setName(ActionChoisirCouleur.NOM_ACTION);
		this.add(buttonColor);
		
		JCheckBox buttonFill = new JCheckBox(new ActionChoisirRemplissage(panneauDessin));
		buttonFill.setName(ActionChoisirRemplissage.NOM_ACTION);
		this.add(buttonFill);
		
	}
	
	public int getNbBranches() {
		return (int) this.nombrebranche.getValue();
	}
	
	public double getLongueurBranche() {
		return (double) this.longeurBranche.getValue();
	}
}