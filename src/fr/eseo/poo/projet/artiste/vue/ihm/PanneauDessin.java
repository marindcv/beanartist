package fr.eseo.poo.projet.artiste.vue.ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import fr.eseo.poo.projet.artiste.controleur.outils.Outil;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

/**
 * <strong> Fonction : <code>PanneauDessin</code> </strong>
 * @author marindecacqueray
 */
@SuppressWarnings("serial")
public class PanneauDessin extends javax.swing.JPanel {

	/*
	 * Accesseurs
	 */
	public static final int LARGEUR_PAR_DEFAUT = 1450;
	public static final int HAUTEUR_PAR_DEFAUT = 1350;
	public static final Color COULEUR_FOND_PAR_DEFAUT = new Color(27, 28, 29);/*Matching default when DarkMode on*/
	
	private final List<VueForme> vueFormes;
	private Outil outilCourant;
	private Color couleurCourante;
	private boolean rempli;
	
	/*
	 * Constructeurs
	 */
	public PanneauDessin() {
		this(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT, COULEUR_FOND_PAR_DEFAUT);
	}
	
	public PanneauDessin(int largeur, int hauteur, Color couleur) {
		this.vueFormes = new ArrayList<VueForme>();
		this.setBackground(couleur);
		this.setPreferredSize(new Dimension(largeur, hauteur));
		this.setCouleurCourante(Forme.COULEUR_PAR_DEFAUT);
		this.setModeRemplissage(false);
	}
	
	/*
	 * Getters & Setters
	 */
	public List<VueForme> getVueFormes(){
		return this.vueFormes;
	}
	
	public void ajouterVueForme(VueForme vueForme) {
		getVueFormes().add(vueForme);
	}
		
	public Outil getOutilCourant() {
		return this.outilCourant;
	}
	
	private void setOutilCourant(Outil outil) {
		this.outilCourant = outil;
	}
	
	public Color getCouleurCourante() {
		return this.couleurCourante;
	}
	
	public void setCouleurCourante(Color color) {
		this.couleurCourante = color;
	}
	
	public boolean getModeRemplissage() {
		return this.rempli;
	}
	
	public void setModeRemplissage(boolean estRempli) {
		this.rempli = estRempli;
	}
	
	/*
	 * Methodes
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D) g.create();
		for(VueForme vue: getVueFormes()) {
			vue.affiche(g2D);
		}
		g2D.dispose();
	}
	
	public void associerOutil(Outil outil) {
		if(outil != null) {
			this.dissocierOutil();
			this.setOutilCourant(outil);
			outil.setPanneauDessin(this);
			this.addMouseListener(outil);
			this.addMouseMotionListener(outil);
		}
	}
	
	private void dissocierOutil() {
		if(this.getOutilCourant() != null) {
			this.getOutilCourant().setPanneauDessin(null);
			this.removeMouseListener(getOutilCourant());
			this.removeMouseMotionListener(getOutilCourant());
			this.setOutilCourant(null);
		}
		
	}
	
}
