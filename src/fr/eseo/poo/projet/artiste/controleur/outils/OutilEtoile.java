package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;
import fr.eseo.poo.projet.artiste.vue.formes.VueEtoile;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;

/**
 * <strong> Fonction : <code>OutilEtoile</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.controleur.outils.OutilForme
 */
public class OutilEtoile extends OutilForme {
	
	/*
	 * Accesseurs
	 */
	private Etoile etoile = null;
	private PanneauBarreOutils panneauOutil;
	
	/*
	 * Constructeurs
	 */
	public OutilEtoile(PanneauBarreOutils panneauBarreOutils) {
		super();
		this.panneauOutil = panneauBarreOutils;
	}
	
	/*
	 * Methodes
	 */
	@Override
	protected VueForme creerVueForme() {
		this.etoile = new Etoile();
		this.etoile.setPosition(this.getDebut());
		if(!this.getDebut().equals(this.getFin())) {
			double length = (this.getDebut().distanceVers(this.getFin()));
			
			this.etoile.setPosition(new Coordonnees(	this.getFin().getAbscisse()-(length),
												this.getFin().getOrdonnee()-(length)));
			
			etoile.setHauteur(this.getDebut().distanceVers(getFin())*2);
			this.etoile.setAnglePremiereBranche(this.etoile.getEtoileCentre().angleVers(getDebut()));
		}
		else {
			this.etoile.setPosition(new Coordonnees(this.getFin().getAbscisse(),
													this.getFin().getOrdonnee()));
		}
		etoile.setNombreBranches(this.panneauOutil.getNbBranches());
		etoile.setLongueurBranche(this.panneauOutil.getLongueurBranche());
		etoile.setCouleur(getPanneauDessin().getCouleurCourante());
		etoile.setRempli(getPanneauDessin().getModeRemplissage());
		return new VueEtoile(etoile);
	}
}
