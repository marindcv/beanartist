package fr.eseo.poo.projet.artiste.controleur.outils;

import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

import fr.eseo.poo.projet.artiste.controleur.actions.ActionSelectionner;
import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

/**
 * <strong> Fonction : <code>OutilSelectionner</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.controleur.outils.Outil
 */
public class OutilSelectionner extends Outil {
	
	/*
	 * Accesseurs
	 */
	Forme formeSelectionnee;
	private Point mouseCoordonnees;
	
	/*
	 * Constructeurs
	 */
	public OutilSelectionner() {
		super();
		if(getFormeSelectionnee() != null) {
			JOptionPane.showMessageDialog(getPanneauDessin(), getFormeSelectionnee().toString(), 
							ActionSelectionner.NOM_ACTION, JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	/*
	 * Methodes
	 */
	@Override
	public void mouseClicked(MouseEvent event) {
		setMouseCoordonnees(event.getPoint());
		for (int i = getPanneauDessin().getVueFormes().size(); i>0; i--) {
			VueForme vueForme = getPanneauDessin().getVueFormes().get(i-1);
			if(vueForme.getForme().contient(
					new Coordonnees(getMouseCoordonnees().getX(), 
									getMouseCoordonnees().getY()))) {
				
				this.setFormeSelectionnee(vueForme.getForme());
				JOptionPane.showMessageDialog(getPanneauDessin(), getFormeSelectionnee().toString(), 
									ActionSelectionner.NOM_ACTION, JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	
	/*
	 * Getters & Setters
	 */
	private void setMouseCoordonnees(Point point) {
		this.mouseCoordonnees = point;
	}
	
	private Point getMouseCoordonnees() {
		return this.mouseCoordonnees;
	}
	
	private void setFormeSelectionnee(Forme formeSelect) {
		this.formeSelectionnee = formeSelect;
	}
	
	private Forme getFormeSelectionnee() {
		return this.formeSelectionnee;
	}
}
