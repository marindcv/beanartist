package fr.eseo.poo.projet.artiste.controleur.outils;

import java.awt.event.MouseEvent;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.vue.formes.VueCercle;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

/**
 * <strong> Fonction : <code>OutilCercle</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.controleur.outils.OutilEllipse
 */
public class OutilCercle extends OutilForme {
	
	/*
	 * Methodes
	 */
	@Override
	public void mouseClicked(MouseEvent event) {
		if(event.getClickCount() == 2) {
			System.err.println("In if condition mouseClicked");
			getPanneauDessin().ajouterVueForme(creerVueForme());
			getPanneauDessin().repaint();
		}
	}
	
	@Override
	protected VueForme creerVueForme() {
		Cercle cercle = new Cercle(this.getDebut());
		if(!this.getDebut().equals(this.getFin())){
			
			double length = Math.max(	Math.abs(this.getFin().getOrdonnee()-this.getDebut().getOrdonnee()), 
										Math.abs(this.getFin().getAbscisse()-this.getDebut().getAbscisse()));
			
			if(	this.getDebut().getAbscisse() > this.getFin().getAbscisse() && 
				this.getDebut().getOrdonnee() > this.getFin().getOrdonnee()) {
				
				double x = this.getDebut().getAbscisse()-length;
				double y = this.getDebut().getOrdonnee()-length;
				cercle.setPosition(new Coordonnees(x,y));
			}
			else if(this.getDebut().getAbscisse() > this.getFin().getAbscisse() && 
					this.getDebut().getOrdonnee() < this.getFin().getOrdonnee()) {
				
				double x = this.getDebut().getAbscisse()-length;
				cercle.setPosition(new Coordonnees(x, getDebut().getOrdonnee()));
			}
			else if(this.getDebut().getAbscisse() < this.getFin().getAbscisse() && 
					this.getDebut().getOrdonnee() > this.getFin().getOrdonnee()) {
				
				double y = this.getDebut().getOrdonnee()-length;
				cercle.setPosition(new Coordonnees(getDebut().getAbscisse(), y));
			}
			cercle.setLargeur(Math.max(Math.abs(this.getFin().getOrdonnee()-this.getDebut().getOrdonnee()), 
										Math.abs(this.getFin().getAbscisse()-this.getDebut().getAbscisse())));
		}
		cercle.setCouleur(getPanneauDessin().getCouleurCourante());
		cercle.setRempli(getPanneauDessin().getModeRemplissage());
		return new VueCercle(cercle);
	}
}


	