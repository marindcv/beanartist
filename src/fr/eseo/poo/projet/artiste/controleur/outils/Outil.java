package fr.eseo.poo.projet.artiste.controleur.outils;

import java.awt.event.MouseEvent;
import javax.swing.event.MouseInputListener;
import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <strong> Fonction : <code>Outil</code> </strong>
 * @author marindecacqueray
 */
public abstract class Outil implements MouseInputListener {
	
	/*
	 * Accesseurs
	 */
	private Coordonnees debut;
	private Coordonnees fin;
	private PanneauDessin panneauDessin;
	
	/*
	 * Getters & Setters
	 */
	public Coordonnees getDebut() {
		return this.debut; 
	}
	
	public void setDebut(Coordonnees debut) {
		this.debut = debut;
	}
	
	public Coordonnees getFin() {
		return this.fin;
	}
	
	public void setFin(Coordonnees fin) {
		this.fin = fin;
	}
	
	public PanneauDessin getPanneauDessin() {
		return this.panneauDessin;
	}
	
	public void setPanneauDessin(PanneauDessin panneauDessin) {
		this.panneauDessin = panneauDessin;
	}
	
	/*
	 * Methodes
	 */
	public void mouseClicked(MouseEvent event) {
		
	}
	
	public void mouseDragged(MouseEvent envent) {
		
	}
	
	public void mouseEntered(MouseEvent event) {
		
	}
	
	public void mouseExited(MouseEvent event) {
		
	}
	
	public void mouseMoved(MouseEvent event) {
		
	}
	
	public void mousePressed(MouseEvent event) {
		this.setDebut(new Coordonnees(event.getX(), event.getY()));
	}
	
	public void mouseReleased(MouseEvent event) {
		this.setFin(new Coordonnees(event.getX(), event.getY()));
	}
}
