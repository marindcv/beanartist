package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.vue.formes.VueEllipse;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

/**
 * <strong> Fonction : <code>OutilEllipse</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.controleur.outils.OutilForme
 */
public class OutilEllipse extends OutilForme{

	/*
	 * Methodes
	 */
	@Override
	protected VueForme creerVueForme() {
		Ellipse ellipse = new Ellipse(getDebut());
		if(!this.getDebut().equals(this.getFin())) {
			if(	this.getDebut().getAbscisse() > this.getFin().getAbscisse() && 
				this.getDebut().getOrdonnee() > this.getFin().getOrdonnee()) {
				
				ellipse.setPosition(getFin());
			}
			else if(this.getDebut().getAbscisse() > this.getFin().getAbscisse() && 
					this.getDebut().getOrdonnee() < this.getFin().getOrdonnee()) {
				
				ellipse.setPosition(new Coordonnees(getFin().getAbscisse(), getDebut().getOrdonnee()));
			}
			else if(this.getDebut().getAbscisse() < this.getFin().getAbscisse() && 
					this.getDebut().getOrdonnee() > this.getFin().getOrdonnee()) {
				
				ellipse.setPosition(new Coordonnees(getDebut().getAbscisse(), getFin().getOrdonnee()));
			}
			ellipse.setHauteur(Math.abs(this.getFin().getOrdonnee()-this.getDebut().getOrdonnee()));
			ellipse.setLargeur(Math.abs(this.getFin().getAbscisse()-this.getDebut().getAbscisse()));
		}
		ellipse.setCouleur(getPanneauDessin().getCouleurCourante());
		ellipse.setRempli(getPanneauDessin().getModeRemplissage());
		return new VueEllipse(ellipse);
	}	
}
