package fr.eseo.poo.projet.artiste.controleur.outils;

import java.awt.event.MouseEvent;

import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

/**
 * <strong> Fonction : <code>OutilForme</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.controleur.outils.Outil
 */
public abstract class OutilForme extends Outil{
	
	@Override
	public void mouseClicked(MouseEvent event) {
		if(event.getClickCount() == 2) {
			System.err.println("In if condition mouseClicked");
			getPanneauDessin().ajouterVueForme(creerVueForme());
			getPanneauDessin().repaint();
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent event) {
		super.mouseReleased(event);
		System.err.println("Mouse released outilForme fin : " + getFin()+"debut : "+getDebut());
		if(super.getDebut().equals(super.getFin())) {
			throw new IllegalArgumentException("Start Coordonate and End Coordonate can't be equal in this case");
		}
		else {
			getPanneauDessin().ajouterVueForme(creerVueForme());
			getPanneauDessin().repaint();
		}
	}
	
	protected abstract VueForme creerVueForme();
	
}
