package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.formes.VueLigne;

/**
 * <strong> Fonction : <code>OutilCercle</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.controleur.outils.OutilForme
 */
public class OutilLigne extends OutilForme{
	
	/*
	 * Methodes
	 */
	@Override
	protected VueForme creerVueForme() {
		Ligne ligne  = new Ligne(getDebut());
		if(!this.getDebut().equals(this.getFin())) {
			ligne.setC2(getFin());
		}
		ligne.setCouleur(getPanneauDessin().getCouleurCourante());
		return new VueLigne(ligne);
	}
}
