package fr.eseo.poo.projet.artiste.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.poo.projet.artiste.controleur.outils.OutilCercle;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilEllipse;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilEtoile;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilLigne;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <strong> Fonction : <code>ActionChoisirForme</code> </strong>
 * @author marindecacqueray
 */
@SuppressWarnings("serial")
public class ActionChoisirForme extends AbstractAction {
	
	/*
	 * Accesseurs
	 */
	public static final String NOM_ACTION_LIGNE = "Ligne";
	public static final String NOM_ACTION_ELLIPSE = "Ellipse";
	public static final String NOM_ACTION_CERCLE = "Cercle";
	public static final String NOM_ACTION_ETOILE = "Etoile";
	private PanneauDessin panneauDessin;
	private PanneauBarreOutils panneauOutil;
	
	/*
	 * Constructeurs
	 */
	public ActionChoisirForme(PanneauDessin panneauDessin, PanneauBarreOutils panneauOutil, String nomAction) {
		super(nomAction);
		this.panneauDessin = panneauDessin;
		this.panneauOutil = panneauOutil;
	}
	
	/*
	 * Methodes
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		String nomAction = event.getActionCommand();
		System.err.println("in ActionPerformed");
		if(nomAction.equals(NOM_ACTION_LIGNE)) {
			OutilLigne monOutil = new OutilLigne();
			this.panneauDessin.associerOutil(monOutil);
			System.err.println("in ActionPerformed creating outilLigne");
		}
		else if(nomAction.equals(NOM_ACTION_ELLIPSE)) {
			OutilEllipse monOutil = new OutilEllipse();
			this.panneauDessin.associerOutil(monOutil);
		}
		else if(nomAction.equals(NOM_ACTION_CERCLE)) {
			OutilCercle monOutil = new OutilCercle();
			this.panneauDessin.associerOutil(monOutil);
		}
		else if(nomAction.equals(NOM_ACTION_ETOILE)) {
			OutilEtoile monOutil = new OutilEtoile(this.panneauOutil);
			this.panneauDessin.associerOutil(monOutil);
		}
	}
}
