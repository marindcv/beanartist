package fr.eseo.poo.projet.artiste.controleur.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JColorChooser;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <strong> Fonction : <code>ActionChoisirCouleur</code> </strong>
 * @author marindecacqueray
 */
@SuppressWarnings("serial")
public class ActionChoisirCouleur extends AbstractAction {
	
	/*
	 * Accesseurs
	 */
	public static final String NOM_ACTION = "Couleur";
	private PanneauDessin panneauDessin;
	
	/*
	 * Constructeurs
	 */
	public ActionChoisirCouleur(PanneauDessin panneauDessin) {
		super(NOM_ACTION);
		this.panneauDessin = panneauDessin;
	}
	
	/*
	 * Methodes
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Color output = JColorChooser.showDialog(panneauDessin, NOM_ACTION, panneauDessin.getCouleurCourante());
		if(output != null) {
			this.panneauDessin.setCouleurCourante(output);
		}
	}
	
}
