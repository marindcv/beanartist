package fr.eseo.poo.projet.artiste.controleur.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <strong> Fonction : <code>ActionEffacer</code> </strong>
 * @author marindecacqueray
 */
@SuppressWarnings("serial")
public class ActionEffacer extends AbstractAction {
	
	/*
	 * Accesseurs
	 */
	public static final String NOM_ACTION = "Effacer Tout";
	private PanneauDessin panneauDessin;
	
	/*
	 * Constructeurs
	 */
	public ActionEffacer(PanneauDessin panneauDessin) {
		super(NOM_ACTION);
		this.panneauDessin = panneauDessin;
	}
	
	/*
	 * Methodes
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		
		int output = JOptionPane.showConfirmDialog(
				this.panneauDessin, "Supprimer toute les formes ?", 
				ActionEffacer.NOM_ACTION, JOptionPane.YES_NO_CANCEL_OPTION);
		if(output == JOptionPane.YES_OPTION) {
			this.panneauDessin.getVueFormes().clear();
			this.panneauDessin.repaint();
		}
	}
}
