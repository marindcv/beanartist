package fr.eseo.poo.projet.artiste.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.poo.projet.artiste.controleur.outils.OutilSelectionner;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <strong> Fonction : <code>ActionSelectionner</code> </strong>
 * @author marindecacqueray
 */
@SuppressWarnings("serial")
public class ActionSelectionner extends AbstractAction {
	
	/*
	 * Accesseurs
	 */
	public static final String NOM_ACTION = "Selectionner";
	private PanneauDessin panneauDessin;
	
	/*
	 * Constructeurs
	 */
	public ActionSelectionner(PanneauDessin panneauDessin) {
		super(NOM_ACTION);
		this.panneauDessin = panneauDessin;
	}
	
	/*
	 * Methodes
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		OutilSelectionner outilSelect = new OutilSelectionner();
		this.panneauDessin.associerOutil(outilSelect);
	}
}
