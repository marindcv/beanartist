package fr.eseo.poo.projet.artiste.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <strong> Fonction : <code>ActionRemplissage</code> </strong>
 * @author marindecacqueray
 */
@SuppressWarnings("serial")
public class ActionChoisirRemplissage extends AbstractAction {
	
	/*
	 * Accesseurs
	 */
	public static final String NOM_ACTION = "Remplir";
	private PanneauDessin panneauDessin;
	
	/*
	 * Constructeurs
	 */
	public ActionChoisirRemplissage(PanneauDessin panneauDessin){
		super(NOM_ACTION);
		this.panneauDessin = panneauDessin;
	}
	
	/*
	 * Methodes
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		AbstractButton abstractButton = (AbstractButton) event.getSource();
		this.panneauDessin.setModeRemplissage(abstractButton.getModel().isSelected());
	}
	
	
}
