package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class VueEtoileTest {
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		VueEtoileTest maVueEtoileTest = new VueEtoileTest();
	}
	
	public VueEtoileTest() {
		testVueEtoile();
	}
	
	public void testVueEtoile() {
		Etoile etoileTest1 = new Etoile();
		etoileTest1.setCouleur(Color.white);
		Etoile etoileTest2 = new Etoile(new Coordonnees(100,25), 40, 5, 2.34, 0.79);
		etoileTest2.setCouleur(Color.white);
		Etoile etoileTest3 = new Etoile(new Coordonnees(260,-5), 100, 6, 2.086, 0.35);
		etoileTest3.setCouleur(Color.white);
		Etoile etoileTest4 = new Etoile(new Coordonnees(600,430), 200, 5, 2.086, 0.80);
		etoileTest4.setCouleur(Color.white);
		etoileTest4.setRempli(true);
		Etoile etoileTest5 = new Etoile(new Coordonnees(300,230));
		etoileTest5.setCouleur(Color.white);
		etoileTest5.setAnglePremiereBranche(3.1);
		etoileTest5.setLongueurBranche(0.2);
		etoileTest5.setNombreBranches(15);
		etoileTest5.setHauteur(90);
		VueEtoile vueEtoileTest1 = new VueEtoile(etoileTest1);
		VueEtoile vueEtoileTest2 = new VueEtoile(etoileTest2);
		VueEtoile vueEtoileTest3 = new VueEtoile(etoileTest3);
		VueEtoile vueEtoileTest4 = new VueEtoile(etoileTest4);
		VueEtoile vueEtoileTest5 = new VueEtoile(etoileTest5);
		PanneauDessin monPanneau = new PanneauDessin();
		monPanneau.ajouterVueForme(vueEtoileTest1);
		monPanneau.ajouterVueForme(vueEtoileTest2);
		monPanneau.ajouterVueForme(vueEtoileTest3);
		monPanneau.ajouterVueForme(vueEtoileTest4);
		monPanneau.ajouterVueForme(vueEtoileTest5);
		JFrame f = new JFrame("Test de la vue Etoile");
		f.add(monPanneau);
		PanneauBarreOutils panneauOutil = new PanneauBarreOutils(monPanneau);
		f.add(panneauOutil, BorderLayout.EAST);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}

