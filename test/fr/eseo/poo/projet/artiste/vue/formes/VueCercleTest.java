package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class VueCercleTest {
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		VueCercleTest maVueCercleTest = new VueCercleTest();
	}
	
	public VueCercleTest() {
		testVueCercle();
	}
	
	public void testVueCercle() {
		Cercle cercleTest1 = new Cercle();
		cercleTest1.setLargeur(1000);
		Cercle cercleTest2 = new Cercle(new Coordonnees(100,25));
		cercleTest2.setLargeur(300);
		Cercle cercleTest3 = new Cercle(new Coordonnees(260,-5));
		cercleTest3.setLargeur(30);
		VueCercle vueCercleTest1 = new VueCercle(cercleTest1);
		VueCercle vueCercleTest2 = new VueCercle(cercleTest2);
		VueCercle vueCercleTest3 = new VueCercle(cercleTest3);
		PanneauDessin monPanneau = new PanneauDessin();
		monPanneau.ajouterVueForme(vueCercleTest1);
		monPanneau.ajouterVueForme(vueCercleTest2);
		monPanneau.ajouterVueForme(vueCercleTest3);
		JFrame f = new JFrame("Test de la vue ligne");
		f.add(monPanneau);
		PanneauBarreOutils panneauOutil = new PanneauBarreOutils(monPanneau);
		f.add(panneauOutil, BorderLayout.EAST);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}