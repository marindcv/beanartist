package fr.eseo.poo.projet.artiste.vue.formes;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class VueEllipseTest {
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		VueEllipseTest maVueEllipseTest = new VueEllipseTest();
	}
	
	public VueEllipseTest() {
		testVueEllipse();
	}
	
	public void testVueEllipse() {
		Ellipse ellipseTest1 = new Ellipse();
		ellipseTest1.setHauteur(1000);
		ellipseTest1.setLargeur(960);
		Ellipse ellipseTest2 = new Ellipse(new Coordonnees(100,25));
		ellipseTest2.setLargeur(300);
		ellipseTest2.setHauteur(400);
		Ellipse ellipseTest3 = new Ellipse(new Coordonnees(260,-5));
		ellipseTest3.setLargeur(30);
		ellipseTest3.setHauteur(50);
		VueEllipse vueEllipseTest1 = new VueEllipse(ellipseTest1);
		VueEllipse vueEllipseTest2 = new VueEllipse(ellipseTest2);
		VueEllipse vueEllipseTest3 = new VueEllipse(ellipseTest3);
		PanneauDessin monPanneau = new PanneauDessin();
		monPanneau.ajouterVueForme(vueEllipseTest1);
		monPanneau.ajouterVueForme(vueEllipseTest2);
		monPanneau.ajouterVueForme(vueEllipseTest3);
		JFrame f = new JFrame("Test de la vue Ellipse");
		f.add(monPanneau);
		PanneauBarreOutils panneauOutil = new PanneauBarreOutils(monPanneau);
		f.add(panneauOutil, BorderLayout.EAST);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}

