package fr.eseo.poo.projet.artiste.vue.formes;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

public class VueLigneTest {
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		VueLigneTest maVueLigneTest = new VueLigneTest();
	}
	
	public VueLigneTest() {
		testVueLigne();
	}
	
	public void testVueLigne() {
		Ligne ligneTest1 = new Ligne();
		ligneTest1.setC2(new Coordonnees(1000,800));
		Ligne ligneTest2 = new Ligne(new Coordonnees(100,25));
		ligneTest2.setC2(new Coordonnees(100,300));
		Ligne ligneTest3 = new Ligne(new Coordonnees(2600,-5));
		ligneTest3.setC2(new Coordonnees(500,600));
		VueLigne vueligneTest1 = new VueLigne(ligneTest1);
		VueLigne vueligneTest2 = new VueLigne(ligneTest2);
		VueLigne vueligneTest3 = new VueLigne(ligneTest3);
		PanneauDessin monPanneau = new PanneauDessin();
		monPanneau.ajouterVueForme(vueligneTest1);
		monPanneau.ajouterVueForme(vueligneTest2);
		monPanneau.ajouterVueForme(vueligneTest3);
		JFrame f = new JFrame("Test de la vue ligne");
		f.add(monPanneau);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
