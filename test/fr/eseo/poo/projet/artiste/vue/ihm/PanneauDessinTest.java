package fr.eseo.poo.projet.artiste.vue.ihm;

import java.awt.Color;
import javax.swing.JFrame;

public class PanneauDessinTest {
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		PanneauDessinTest monPanneau = new PanneauDessinTest();
	}
	
	public PanneauDessinTest() {
		testConstructeurParDefaut();
		testConstructeur();
	}
	
	private void testConstructeurParDefaut() {
		JFrame f = new JFrame("Etre un Artiste");
		f.add(new PanneauDessin());
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void testConstructeur() {
		JFrame f = new JFrame("Blues du Businessman");
		f.add(new PanneauDessin(1240,120,new Color(238,238,238)));
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
