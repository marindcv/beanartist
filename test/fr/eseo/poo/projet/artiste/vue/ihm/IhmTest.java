package fr.eseo.poo.projet.artiste.vue.ihm;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.formes.VueLigne;

public class IhmTest {
	public IhmTest() {
	    testPanneau();
	  }
	  public void testPanneau(){
		  Ligne ligneTest1 = new Ligne();
			VueLigne vueligneTest1 = new VueLigne(ligneTest1);
			PanneauDessin monPanneau = new PanneauDessin();
			monPanneau.ajouterVueForme(vueligneTest1);
			JFrame f = new JFrame("Test de la vue ligne");
			f.add(monPanneau);
			f.pack();
			f.setLocationRelativeTo(null);
			f.setVisible(true);
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  }
	public static void main(String [] args) { SwingUtilities.invokeLater(new Runnable(){
	      @Override
	      public void run() {
	        new IhmTest();
	      }
	}); }
	}