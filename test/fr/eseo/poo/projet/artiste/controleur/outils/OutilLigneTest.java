package fr.eseo.poo.projet.artiste.controleur.outils;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <h1> Test </h1>
 * <strong> Fonction : <code>OutilLigneTest</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.controleur.outils.OutilLigne
 */
public class OutilLigneTest {

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		OutilLigneTest outilLigneTest = new OutilLigneTest();
	}
	
	public OutilLigneTest() {
		testOutilLigne();
	}
	
	public void testOutilLigne() {
		OutilLigne testOutiLigne = new OutilLigne();
		PanneauDessin monPanneau = new PanneauDessin();
		monPanneau.associerOutil(testOutiLigne);
		JFrame f = new JFrame("Test de l'Outil ligne");
		f.add(monPanneau);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
