package fr.eseo.poo.projet.artiste.controleur.outils;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <h1> Test </h1>
 * <strong> Fonction : <code>OutilCercleTest</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.controleur.outils.OutilCercle
 */
public class OutilCercleTest {
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		OutilCercleTest outilCercleTest = new OutilCercleTest();
	}
	
	public OutilCercleTest() {
		testOutilCercle();
	}
	
	public void testOutilCercle() {
		OutilCercle testOutilCercle = new OutilCercle();
		PanneauDessin monPanneau = new PanneauDessin();
		monPanneau.associerOutil(testOutilCercle);
		JFrame f = new JFrame("Test de l'Outil Cercle");
		f.add(monPanneau);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
