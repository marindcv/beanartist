package fr.eseo.poo.projet.artiste.controleur.outils;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <h1> Test </h1>
 * <strong> Fonction : <code>OutilCercleTest</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.controleur.outils.OutilCercle
 */
public class OutilEtoileTest {
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		OutilEtoileTest outilEtoileTest = new OutilEtoileTest();
	}
	
	public OutilEtoileTest() {
		testOutilEtoile();
	}
	
	public void testOutilEtoile() {
		PanneauDessin monPanneau = new PanneauDessin();
		PanneauBarreOutils panneauOutil = new PanneauBarreOutils(monPanneau);
		OutilEtoile testOutilEtoile = new OutilEtoile(panneauOutil);
		monPanneau.associerOutil(testOutilEtoile);
		JFrame f = new JFrame("Test de l'Outil Etoile");
		f.add(monPanneau);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
