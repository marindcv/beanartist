package fr.eseo.poo.projet.artiste.controleur.outils;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <h1> Test </h1>
 * <strong> Fonction : <code>OutilEllipseTest</code> </strong>
 * @author marindecacqueray
 * @see fr.eseo.poo.projet.artiste.controleur.outils.OutilEllipse
 */
public class OutilEllipseTest {
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		OutilEllipseTest outilEllipseTest = new OutilEllipseTest();
	}
	
	public OutilEllipseTest() {
		testOutilEllipse();
	}
	
	public void testOutilEllipse() {
		OutilEllipse testOutilEllipse = new OutilEllipse();
		PanneauDessin monPanneau = new PanneauDessin();
		monPanneau.associerOutil(testOutilEllipse);
		JFrame f = new JFrame("Test de l'Outil Ellipse");
		f.add(monPanneau);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
