package fr.eseo.poo.projet.artiste.controleur.actions;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.formes.VueLigne;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

/**
 * <h1> Test </h1>
 * <strong> Fonction <code>ActionChoisirCouleurTest</code> </strong>
 * @author marindecacqueray
 * @see src.fr.eseo.projet.artiste.controleur.actions.ActionChoisirCouleur
 */
public class ActionChoisirCouleurTest {
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		ActionChoisirCouleurTest actionChoisirCouleurTest = new ActionChoisirCouleurTest();
	}
	
	public ActionChoisirCouleurTest() {
		testChoisirCouleur();
	}
	
	public void testChoisirCouleur() {
		Ligne ligneTest1 = new Ligne();
		ligneTest1.setC2(new Coordonnees(100,800));
		Ligne ligneTest2 = new Ligne(new Coordonnees(100,25));
		ligneTest2.setC2(new Coordonnees(100,300));
		Ligne ligneTest3 = new Ligne(new Coordonnees(600,-5));
		ligneTest3.setC2(new Coordonnees(500,600));
		VueLigne vueligneTest1 = new VueLigne(ligneTest1);
		VueLigne vueligneTest2 = new VueLigne(ligneTest2);
		VueLigne vueligneTest3 = new VueLigne(ligneTest3);
		PanneauDessin panneauDessin = new PanneauDessin();
		panneauDessin.ajouterVueForme(vueligneTest1);
		panneauDessin.ajouterVueForme(vueligneTest2);
		panneauDessin.ajouterVueForme(vueligneTest3);
		JFrame f = new JFrame("Test de l'effaceur");
		f.add(panneauDessin);
		PanneauBarreOutils panneauOutil = new PanneauBarreOutils(panneauDessin);
		f.add(panneauOutil, BorderLayout.EAST);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
