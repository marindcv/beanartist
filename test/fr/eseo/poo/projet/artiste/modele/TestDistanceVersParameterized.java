package fr.eseo.poo.projet.artiste.modele;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.eseo.poo.projet.artiste.modele.formes.Ligne;

@RunWith(Parameterized.class)
public class TestDistanceVersParameterized {
	
	Coordonnees coordonnees1 = new Coordonnees();
	Coordonnees coordonnees2 = new Coordonnees();
	double valeurAttendu;
	
	public TestDistanceVersParameterized(Coordonnees coordonnees1, Coordonnees coordonnees2, double valeurAttendus) {
		this.coordonnees1 = coordonnees1;
		this.coordonnees2 = coordonnees2;
		this.valeurAttendu = valeurAttendus;	
	}
	
	@Parameters(name = "moAngle[{index}] : {0}, {1}, {2}") 
	public static Collection<Object[]> distanceVers(){
		final Object[][] laForme = new Object[][] {
			{new Coordonnees(20, 30),new Coordonnees(50,70),50},
			{new Coordonnees(100,100),new Coordonnees(20, 40),100},
			{new Coordonnees(14,114),new Coordonnees(309, 810),755.937},
			{new Coordonnees(209,349),new Coordonnees(517, 712),476.059},
			{new Coordonnees(20,920),new Coordonnees(70, 298),624.006},
			{new Coordonnees(473,287),new Coordonnees(576, 491),228.527},
			{new Coordonnees(641,121),new Coordonnees(49, 935),1006.508},
			{new Coordonnees(445,289),new Coordonnees(675, 528),331.694},
			{new Coordonnees(697,511),new Coordonnees(381, 977),563.0381},
			{new Coordonnees(261,919),new Coordonnees(58, 530),438.782},
			{new Coordonnees(2,641),new Coordonnees(156, 386),297.894},
			{new Coordonnees(628,892),new Coordonnees(986, 4),957.448}
		};
		return Arrays.asList(laForme);
	}
	
	@Test
	public void angleTest() {
		
		Ligne laForme = new Ligne(coordonnees1);
		laForme.setC2(coordonnees2);
		
		assertEquals(valeurAttendu,coordonnees1.distanceVers(coordonnees2),0.001);
	}
	
}
