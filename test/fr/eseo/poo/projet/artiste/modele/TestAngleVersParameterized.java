package fr.eseo.poo.projet.artiste.modele;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.eseo.poo.projet.artiste.modele.formes.Ligne;

@RunWith(Parameterized.class)
public class TestAngleVersParameterized {
	

	Coordonnees coordonnees1 = new Coordonnees();
	Coordonnees coordonnees2 = new Coordonnees();
	double valeurAttendu;
	
	public TestAngleVersParameterized(Coordonnees coordonnees1, Coordonnees coordonnees2, double valeurAttendus) {
		this.coordonnees1 = coordonnees1;
		this.coordonnees2 = coordonnees2;
		this.valeurAttendu = valeurAttendus;
		
		
	}
	
	@Parameters(name = "moAngle[{index}] : {0}, {1}, {2}") 
	public static Collection<Object[]> angleVers(){
		final Object[][] laForme = new Object[][] {
			{new Coordonnees(20, 30),new Coordonnees(50,70),0.927},
			{new Coordonnees(100,100),new Coordonnees(20, 40),-2.498},
			{new Coordonnees(14,114),new Coordonnees(309, 810),1.169},
			{new Coordonnees(209,349),new Coordonnees(517, 712),0.867},
			{new Coordonnees(20,920),new Coordonnees(70, 298),-1.490},
			{new Coordonnees(473,287),new Coordonnees(576, 491),1.103},
			{new Coordonnees(641,121),new Coordonnees(49, 935),2.199},
			{new Coordonnees(445,289),new Coordonnees(675, 528),0.804},
			{new Coordonnees(697,511),new Coordonnees(381, 977),2.166},
			{new Coordonnees(261,919),new Coordonnees(58, 530),-2.051},
			{new Coordonnees(2,641),new Coordonnees(156, 386),-1.027},
			{new Coordonnees(628,892),new Coordonnees(986, 4),-1.187}
		};
		return Arrays.asList(laForme);
	}
	
	/*
	 * If need be..
	 */
	@Before
	public void setup() {
		/**
		 * Not in use here
		 */
	}
		
	@Test
	public void angleTest() {
		
		Ligne laForme = new Ligne(coordonnees1);
		laForme.setC2(coordonnees2);
		
		assertEquals(valeurAttendu,coordonnees1.angleVers(coordonnees2),0.001);
	}

}
