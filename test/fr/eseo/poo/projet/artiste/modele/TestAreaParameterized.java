package fr.eseo.poo.projet.artiste.modele;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.modele.formes.Ligne;

@RunWith(Parameterized.class)
public class TestAreaParameterized {
	
	private Forme forme;
	private Coordonnees coordonnees;
	private double hauteur;
	private double largeur;
	public double aire;
	
	public TestAreaParameterized(Forme laForme, Coordonnees lesCoordonnees, double laHauteur, double lalargeur, double lAire) {
		this.forme = laForme;
		this.coordonnees = lesCoordonnees;
		this.hauteur = laHauteur;
		this.largeur = lalargeur;
		this.aire = lAire;
	}
	
	@Parameters(name = "this Area[{index}] : {0}, {1}, {2}, {3}, {4}, {5}")
	public static Collection<Object[]> aire(){
		final Object[][] maForme = new Object[][] {
			{new Cercle(),new Coordonnees(20,10), 0,100,7853.981},
			{new Ellipse(), new Coordonnees(100,120), 200,39,6126.105},
			{new Etoile(),new Coordonnees(10,999), 54,78,2235.053},
			{new Ligne(),new Coordonnees(703,2), 23,-25,0},
			{new Cercle(),new Coordonnees(517,83), 0,72,4071.504},
			{new Ellipse(),new Coordonnees(20,100), 30,10,235.619},
			{new Etoile(),new Coordonnees(198,11), 25,90,2975.662},
			{new Ligne(),new Coordonnees(605,180), 30,100,0},
			{new Cercle(),new Coordonnees(517,83), 0,72,4071.504},
			{new Ellipse(),new Coordonnees(125,419), 62,15,730.420},
			{new Etoile(),new Coordonnees(64,277), 12,26,248.339}
		};
		return Arrays.asList(maForme);
	}
	
	@Test 
	public void testUserId(){
		Forme maForme = forme;
		maForme.setPosition(coordonnees);
		maForme.setHauteur(hauteur);
		maForme.setLargeur(largeur);
		
		assertEquals(aire, maForme.aire(), 0.001);
		
	}

}
