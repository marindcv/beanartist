package fr.eseo.poo.projet.artiste.modele;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CoordonneesTest {
	
	public static final double EPSILON = 0.001;
	
	double x1 = 1;
	double y1 = 0;
	double x2 = 100.5;
	double y2 = 87.67;
	double x3 = -43.55;
	double y3 = -99;

	/**
	 * Testing the first constructor
	 */
	@Test
	public void constructeurTest1() {
	
		Coordonnees mesCoordonnee1 = new Coordonnees(x1,y1);
		Coordonnees mesCoordonnee2 = new Coordonnees(x2,y2);
		Coordonnees mesCoordonnee3 = new Coordonnees(x3,y3);
		
		assertEquals(x1,mesCoordonnee1.getAbscisse(),EPSILON);
		assertEquals(y1,mesCoordonnee1.getOrdonnee(),EPSILON);
		assertEquals(x2,mesCoordonnee2.getAbscisse(),EPSILON);
		assertEquals(y2,mesCoordonnee2.getOrdonnee(),EPSILON);
		assertEquals(x3,mesCoordonnee3.getAbscisse(),EPSILON);
		assertEquals(y3,mesCoordonnee3.getOrdonnee(),EPSILON);
	}
	
	/**
	 * Testing the second constructor
	 */
	@Test
	public void constructeurTest2() {
		Coordonnees mesCoordonnee2 = new Coordonnees();
		
		assertEquals(Coordonnees.ABSCISSE_PAR_DEFAUT,mesCoordonnee2.getAbscisse(),EPSILON);
		assertEquals(Coordonnees.ORDONNEE_PAR_DEFAUT,mesCoordonnee2.getOrdonnee(),EPSILON);
	}
	
	/**
	 * Testing the setAbscisse
	 */
	@Test
	public void  setAbscisseTest() {
		Coordonnees mesCoordonnee1 = new Coordonnees();
		Coordonnees mesCoordonnee2 = new Coordonnees();
		Coordonnees mesCoordonnee3 = new Coordonnees();
		
		mesCoordonnee1.setAbscisse(x1);
		mesCoordonnee2.setAbscisse(x2);
		mesCoordonnee3.setAbscisse(x3);
		
		
		assertEquals(x1,mesCoordonnee1.getAbscisse(),EPSILON);
		assertEquals(x2,mesCoordonnee2.getAbscisse(),EPSILON);
		assertEquals(x3,mesCoordonnee3.getAbscisse(),EPSILON);
	}
	
	/**
	 * Testing the setOrdonnee
	 */
	@Test
	public void  setOrdonneeTest() {
		Coordonnees mesCoordonnee1 = new Coordonnees();
		Coordonnees mesCoordonnee2 = new Coordonnees();
		Coordonnees mesCoordonnee3 = new Coordonnees();
		
		mesCoordonnee1.setOrdonnee(y1);
		mesCoordonnee2.setOrdonnee(y2);
		mesCoordonnee3.setOrdonnee(y3);
		
		
		assertEquals(y1,mesCoordonnee1.getOrdonnee(),EPSILON);
		assertEquals(y2,mesCoordonnee2.getOrdonnee(),EPSILON);
		assertEquals(y3,mesCoordonnee3.getOrdonnee(),EPSILON);
	}
	
	@Test
	public void deplacerVersTest() {
		Coordonnees mesCoordonnee1 = new Coordonnees(x1,y1);
		Coordonnees mesCoordonnee2 = new Coordonnees(x2,y2);
		Coordonnees mesCoordonnee3 = new Coordonnees(x3,y3);
		
		mesCoordonnee1.deplacerVers(x2, y2);
		mesCoordonnee2.deplacerVers(x3, y3);
		mesCoordonnee3.deplacerVers(x1, y1);
		
		assertEquals(x2,mesCoordonnee1.getAbscisse(),EPSILON);
		assertEquals(x3,mesCoordonnee2.getAbscisse(),EPSILON);
		assertEquals(x1,mesCoordonnee3.getAbscisse(),EPSILON);
		
		assertEquals(y2,mesCoordonnee1.getOrdonnee(),EPSILON);
		assertEquals(y3,mesCoordonnee2.getOrdonnee(),EPSILON);
		assertEquals(y1,mesCoordonnee3.getOrdonnee(),EPSILON);
	}
	
	@Test
	public void deplacerDeTest() {
		Coordonnees mesCoordonnee1 = new Coordonnees(x1,y1);
		Coordonnees mesCoordonnee2 = new Coordonnees(x2,y2);
		Coordonnees mesCoordonnee3 = new Coordonnees(x3,y3);
		
		mesCoordonnee1.deplacerDe(x2, y2);
		mesCoordonnee2.deplacerDe(x3, y3);
		mesCoordonnee3.deplacerDe(x1, y1);
		
		assertEquals(x1+x2,mesCoordonnee1.getAbscisse(),EPSILON);
		assertEquals(x2+x3,mesCoordonnee2.getAbscisse(),EPSILON);
		assertEquals(x3+x1,mesCoordonnee3.getAbscisse(),EPSILON);
		
		assertEquals(y1+y2,mesCoordonnee1.getOrdonnee(),EPSILON);
		assertEquals(y2+y3,mesCoordonnee2.getOrdonnee(),EPSILON);
		assertEquals(y3+y1,mesCoordonnee3.getOrdonnee(),EPSILON);
	}
	
	@Test
	public void distanceVersTest() {
		Coordonnees mesCoordonnee1 = new Coordonnees(x1,y1);
		Coordonnees mesCoordonnee2 = new Coordonnees(x2,y2);
		Coordonnees mesCoordonnee3 = new Coordonnees(x3,y3);
		
		assertEquals(Math.sqrt(Math.pow((x1-x2),2) + (Math.pow((y1 -y2),2))),mesCoordonnee1.distanceVers(mesCoordonnee2),EPSILON);
		assertEquals(Math.sqrt(Math.pow((x2-x3),2) + (Math.pow((y2 -y3),2))),mesCoordonnee2.distanceVers(mesCoordonnee3),EPSILON);
		assertEquals(Math.sqrt(Math.pow((x3-x1),2) + (Math.pow((y3 -y1),2))),mesCoordonnee3.distanceVers(mesCoordonnee1),EPSILON);	
	}
	
	@Test
	public void angleVersTest() {
		Coordonnees mesCoordonnee1 = new Coordonnees(x1,y1);//x = 1 y = 0
		Coordonnees mesCoordonnee2 = new Coordonnees(x2,y2);//x = 100.5 y = 87.67
		Coordonnees mesCoordonnee3 = new Coordonnees(x3,y3);//x = -43.55 y = -99
		
		assertEquals(-Math.atan2(	mesCoordonnee1.getOrdonnee() - 
									mesCoordonnee2.getOrdonnee(), 
									mesCoordonnee2.getAbscisse() - 
									mesCoordonnee1.getAbscisse()),
									mesCoordonnee1.angleVers(mesCoordonnee2),EPSILON);
		assertEquals(-Math.atan2(	mesCoordonnee2.getOrdonnee() - 
									mesCoordonnee3.getOrdonnee(), 
									mesCoordonnee3.getAbscisse() - 
									mesCoordonnee2.getAbscisse()),
									mesCoordonnee2.angleVers(mesCoordonnee3),EPSILON);
		assertEquals(-Math.atan2(	mesCoordonnee3.getOrdonnee() - 
									mesCoordonnee1.getOrdonnee(), 
									mesCoordonnee1.getAbscisse() - 
									mesCoordonnee3.getAbscisse()),
									mesCoordonnee3.angleVers(mesCoordonnee1),EPSILON);
	}
	
	@Test
	public void toStringTest() {
		Coordonnees mesCoordonnee1 = new Coordonnees(x1,y1);
		Coordonnees mesCoordonnee2 = new Coordonnees(x2,y2);
		Coordonnees mesCoordonnee3 = new Coordonnees(x3,y3);
		
		assertEquals("("+x1+" , "+y1+")",mesCoordonnee1.toString());
		assertEquals("("+x2+" , "+y2+")",mesCoordonnee2.toString());
		assertEquals("("+x3+" , "+y3+")",mesCoordonnee3.toString());
	}
	
	

}
