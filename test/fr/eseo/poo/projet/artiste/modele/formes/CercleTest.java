package fr.eseo.poo.projet.artiste.modele.formes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;

/**
 * <h1> Test </h1>
 * <strong> Fonction <code>CercleTest</code> </strong>
 * @author marindecacqueray
 * @see src.fr.eseo.projet.artiste.controleur.modele.forme.Cercle
 */
public class CercleTest {

	public static final double EPSILON = 0.001;
	
	double x1 = 10;
	double y1 = 19.54;
	double largeur1 = 23.5;
	double hauteur1 = 37.99;
	double x2 = 28.88;
	double y2 = -7; 
	
	@Test
	public void constructeurTest1() {
		Cercle monCercle = new Cercle();
		
		assertEquals(Forme.LARGEUR_PAR_DEFAUT, monCercle.getLargeur(),EPSILON);
		assertEquals(new Coordonnees(), monCercle.getPosition());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructeurTest2Failed() throws Exception {
		new Cercle(-0.2);
	}
	
	@Test
	public void constructeurTest2() {
		Cercle monCercle = new Cercle(new Coordonnees(x1,y1));
		
		assertEquals(new Coordonnees(x1,y1),monCercle.getPosition());
		assertEquals(Forme.LARGEUR_PAR_DEFAUT, monCercle.getLargeur(),EPSILON);
	}
	
	@Test
	public void constructeurTest3() {
		Cercle monCercle = new Cercle(largeur1);
		
		assertEquals(new Coordonnees(), monCercle.getPosition());
		assertEquals(largeur1, monCercle.getLargeur(),EPSILON);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void onstructeurTest4Failed() throws Exception {
		new Cercle(new Coordonnees(), -10);
	}
	
	@Test
	public void constructeurTest4() {
		Cercle monCercle = new Cercle(new Coordonnees(x1,y1), largeur1);
		
		assertEquals(new Coordonnees(x1,y1),monCercle.getPosition());
		assertEquals(largeur1, monCercle.getLargeur(),EPSILON);
	}
	
	@Test
	public void toStringTest() {
		Cercle monCercle = new Cercle();
		
		String expectedString = "[Cercle] : pos (0.0 , 0.0) "
				+ "dim 100.0 x 100.0 "
				+ "périmètre : 314.16 "
				+ "aire : 7853.98"
				+ " couleur = R0,G0,B0";
		
		assertEquals(expectedString,monCercle.toString());
	}
	
	@Test
	public void aireTest() {
		Cercle monCercle = new Cercle(largeur1);// = 23.5
		
		assertEquals(433.7361357, monCercle.aire(),EPSILON);//433.7361357 = PI*r^2
	}
	
	@Test
	public void perimetreTest() {
		Cercle monCercle = new Cercle(largeur1);
		
		assertEquals(2*(Math.PI*(largeur1/2)),monCercle.perimetre(),EPSILON);
	}
	
	@Test
	public void contientTest() {
		Cercle monCercle = new Cercle(new Coordonnees(x1, y1), largeur1);//x1 = 10 y1 = 19.54 l = 23.5
		Coordonnees p1 = new Coordonnees(-13.5, 45);
		Coordonnees p2 = new Coordonnees(12, 24.54);
		
		Cercle monCercle2 = new Cercle();
		
		assertFalse(monCercle.contient(p1));
		assertFalse(monCercle.contient(p2));
		
		assertTrue(monCercle2.contient(p2));
		assertTrue(monCercle2.contient(p2));
	}
	
	@Test
	public void estRempliTest() {
		Cercle monCercle = new Cercle();
		
		assertFalse(monCercle.estRempli());
		
		monCercle.setRempli(true);
		
		assertTrue(monCercle.estRempli());
	}
	
	@Test
	public void setLargeurTest() {
		Cercle monCercle = new Cercle();
		monCercle.setLargeur(largeur1);
		
		assertEquals(largeur1, monCercle.getLargeur(), EPSILON);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setLargeurTestFailed() throws Exception {
		Cercle monCercle = new Cercle();
		monCercle.setLargeur(-29);
	}
	
	@Test
	public void setHauteurTest() {
		Cercle monCercle = new Cercle();
		monCercle.setHauteur(hauteur1);
		
		assertEquals(hauteur1, monCercle.getHauteur(), EPSILON);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setHauteurTestFailed() throws Exception {
		Cercle monCercle = new Cercle();
		monCercle.setHauteur(-30);
	}
}
