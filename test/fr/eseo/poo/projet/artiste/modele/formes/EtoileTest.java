package fr.eseo.poo.projet.artiste.modele.formes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;

import org.junit.Test;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;

/**
 * <h1> Test </h1>
 * <strong> Fonction <code>EtoileTest</code> </strong>
 * @author marindecacqueray
 * @see src.fr.eseo.projet.artiste.controleur.modele.forme.Etoile
 */
public class EtoileTest {
	
	public static final double EPSILON = 0.001;
	
	
	/*
	 * Constructeurs Test
	 */
	@Test
	public void constructeur1Test() {
		Etoile monEtoile = new Etoile();
		
		assertEquals(new Coordonnees(), monEtoile.getPosition());
		assertEquals(Etoile.NOMBRE_BRANCHES_PAR_DEFAUT, monEtoile.getNombreBranches());
		assertEquals(Etoile.ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT,monEtoile.getAnglePremiereBranche(),EPSILON);
		assertEquals(Etoile.LONGUEUR_BRANCHE_PAR_DEFAUT,monEtoile.getLongueurBranche(),EPSILON);
		
	}
	
	@Test
	public void constructeur2Test() {
		double taille =  2;
		Etoile monEtoile = new Etoile(taille);
		
		assertEquals(new Coordonnees(), monEtoile.getPosition());
		assertEquals(Etoile.NOMBRE_BRANCHES_PAR_DEFAUT, monEtoile.getNombreBranches());
		assertEquals(Etoile.ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT,monEtoile.getAnglePremiereBranche(),EPSILON);
		assertEquals(Etoile.LONGUEUR_BRANCHE_PAR_DEFAUT,monEtoile.getLongueurBranche(),EPSILON);
		
	}
	
	@Test
	public void constructeur3Test() {
		double x = 100;
		double y = 300;
		Etoile monEtoile = new Etoile(new Coordonnees(x,y));
		
		assertEquals(new Coordonnees(x,y), monEtoile.getPosition());
		assertEquals(Etoile.NOMBRE_BRANCHES_PAR_DEFAUT, monEtoile.getNombreBranches());
		assertEquals(Etoile.ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT,monEtoile.getAnglePremiereBranche(),EPSILON);
		assertEquals(Etoile.LONGUEUR_BRANCHE_PAR_DEFAUT,monEtoile.getLongueurBranche(),EPSILON);
		
	}
	
	@Test
	public void constructeur4Test() {
		double x = 100;
		double y = 300;
		double taille =  2;
		Etoile monEtoile = new Etoile(new Coordonnees(x,y), taille);
		
		assertEquals(new Coordonnees(x,y), monEtoile.getPosition());
		assertEquals(Etoile.NOMBRE_BRANCHES_PAR_DEFAUT, monEtoile.getNombreBranches());
		assertEquals(Etoile.ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT,monEtoile.getAnglePremiereBranche(),EPSILON);
		assertEquals(Etoile.LONGUEUR_BRANCHE_PAR_DEFAUT,monEtoile.getLongueurBranche(),EPSILON);
		
	}
	
	@Test
	public void constructeur5Test() {
		double x = 100;
		double y = 300;
		double taille =  2;
		int nbBranche = 5;
		double angle = 3.124;
		double longueurBranche = 0.78;
		Etoile monEtoile = new Etoile(new Coordonnees(x,y), taille, nbBranche, angle, longueurBranche);
		
		assertEquals(new Coordonnees(x,y), monEtoile.getPosition());
		assertEquals(nbBranche, monEtoile.getNombreBranches());
		assertEquals(angle,monEtoile.getAnglePremiereBranche(),EPSILON);
		assertEquals(longueurBranche,monEtoile.getLongueurBranche(),EPSILON);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructeurTestFailed() throws Exception {	
		double taille =  0;
		new Etoile(taille);	
	}
	
	/*
	 * Setter Test
	 */
	@Test
	public void setNombreBrancheTestOK() {
		
		Random rand = new Random();
		
		Etoile monEtoile = new Etoile();
		monEtoile.setNombreBranches(rand.nextInt(15-3)+3);
		
		assertTrue(monEtoile.getNombreBranches() <= 15 && monEtoile.getNombreBranches() >= 0);
		
		monEtoile.setNombreBranches(rand.nextInt(15-3)+3);
		
		assertTrue(monEtoile.getNombreBranches() <= 15 && monEtoile.getNombreBranches() >= 0);
		
		monEtoile.setNombreBranches(rand.nextInt(15-3)+3);
		
		assertTrue(monEtoile.getNombreBranches() <= 15 && monEtoile.getNombreBranches() >= 0);
		
		monEtoile.setNombreBranches(rand.nextInt(15-3)+3);
		
		assertTrue(monEtoile.getNombreBranches() <= 15 && monEtoile.getNombreBranches() >= 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setNombreBrancheTestFailed() throws Exception {
		
		Random random = new Random();
		
		int wrongNbBrancheOver = random.nextInt(1016)+16;
		int wrongNbBrancheUnder = -random.nextInt(1000);
		
		int rand = random.nextInt(1);
		
		Etoile monEtoile = new Etoile();
		if(rand == 1) {
			monEtoile.setNombreBranches(wrongNbBrancheOver);
		}
		else {
			monEtoile.setNombreBranches(wrongNbBrancheUnder);
		}
		
	}
	
	@Test
	public void setAnglePremiereBrancheTestOK() {
		
		double piApproximation = Math.PI;
		
		Etoile monEtoile = new Etoile();
		monEtoile.setAnglePremiereBranche(Math.random()*(piApproximation - piApproximation)+1);
		
		assertTrue(monEtoile.getAnglePremiereBranche() <= Math.PI && monEtoile.getAnglePremiereBranche() > -Math.PI);
		
		monEtoile.setAnglePremiereBranche(Math.random()*(piApproximation- piApproximation)+1);
		
		assertTrue(monEtoile.getAnglePremiereBranche() <= Math.PI && monEtoile.getAnglePremiereBranche() > -Math.PI);
		
		monEtoile.setAnglePremiereBranche(Math.random()*(piApproximation- piApproximation)+1);
		
		assertTrue(monEtoile.getAnglePremiereBranche() <= Math.PI && monEtoile.getAnglePremiereBranche() > -Math.PI);
		
		monEtoile.setAnglePremiereBranche(Math.random()*(piApproximation- piApproximation)+1);
		
		assertTrue(monEtoile.getAnglePremiereBranche() <= Math.PI && monEtoile.getAnglePremiereBranche() > -Math.PI);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setAnglePremiereBrancheTestFailed() throws Exception {
		
		Random random = new Random();
		
		double piApproximation = Math.PI;
		
		double wrongAngleOver =  (Math.random()*(1000 - (piApproximation + 0.001))+1);
		double wrongAngleUnder = (Math.random()*(-Math.PI -1000)+1);
		
		int rand = random.nextInt(1);
		
		Etoile monEtoile = new Etoile();
		if(rand == 1) {
			monEtoile.setAnglePremiereBranche(wrongAngleOver);
		}
		else {
			monEtoile.setAnglePremiereBranche(wrongAngleUnder);
		}
		
	}
	
	@Test
	public void setLongueurBrancheTestOK() {
		
		Etoile monEtoile = new Etoile();
		monEtoile.setLongueurBranche(Math.random());
		
		assertTrue(monEtoile.getLongueurBranche() <= 1 && monEtoile.getLongueurBranche() >= 0);
		
		monEtoile.setLongueurBranche(Math.random());
		
		assertTrue(monEtoile.getLongueurBranche() <= 1 && monEtoile.getLongueurBranche() >= 0);
		
		monEtoile.setLongueurBranche(Math.random());
		
		assertTrue(monEtoile.getLongueurBranche() <= 1 && monEtoile.getLongueurBranche() >= 0);
		
		monEtoile.setLongueurBranche(Math.random());
		
		assertTrue(monEtoile.getLongueurBranche() <= 1 && monEtoile.getLongueurBranche() >= 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setLongueurBrancheTestFailed1() {
		
		Random random = new Random();
		
		double wrongLengthOver = (Math.random()*(1000 -999)+1);
		double wrongLengthUnder = (Math.random()*(-2 - 1000)+1);
		
		int rand = random.nextInt(1);
		
		Etoile monEtoile = new Etoile();
		if(rand ==1) {
			monEtoile.setLongueurBranche(wrongLengthOver);
		}
		else {
			monEtoile.setLongueurBranche(wrongLengthUnder);
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setLongueurBrancheTestFailed2() {
		
		@SuppressWarnings("unused")
		Etoile monEtoile = new Etoile(new Coordonnees(), 100, 5, 0, -0.001);
	
	}
	
	
	@Test
	public void aireTest() {
		Etoile monEtoile = new Etoile();
		
		assertEquals(3673.657, monEtoile.aire(), EPSILON);
		
		monEtoile.setHauteur(90);
		
		assertEquals(2975.662, monEtoile.aire(), EPSILON);
		
		monEtoile.setNombreBranches(15);
		
		assertEquals(3157.658, monEtoile.aire(), EPSILON);
		
		monEtoile.setLongueurBranche(0.999);
		
		assertEquals(6.315, monEtoile.aire(), EPSILON);
	}
	
	@Test
	public void perimetreTest() {
		Etoile monEtoile = new Etoile();
		
		assertEquals(332.032, monEtoile.perimetre(), EPSILON);
		
		monEtoile.setHauteur(90);
		
		assertEquals(298.829, monEtoile.perimetre(), EPSILON);
		
		monEtoile.setNombreBranches(15);
		
		assertEquals(703.882, monEtoile.perimetre(), EPSILON);
		
		monEtoile.setLongueurBranche(1);
		
		assertEquals(1350, monEtoile.perimetre(), EPSILON);
	}
	
	@Test
	public void contientTest() {
		Etoile monEtoile = new Etoile(new Coordonnees(200, 200), 100, 15, 0, 0);
		
		assertTrue(monEtoile.contient(new Coordonnees(250,250)));
		
		assertFalse(monEtoile.contient(new Coordonnees(200,200)));
	}
	
	/*
	 * Methode test
	 */
	@Test
	public void toStringTest() {
		Etoile monEtoile = new Etoile();
		Locale currentLocale = Locale.getDefault();
		DecimalFormat current = (DecimalFormat)DecimalFormat.getNumberInstance(currentLocale);
		current.setMinimumFractionDigits(1);
		current.setMaximumFractionDigits(2);
		
		String expectedString = "[Etoile] : pos (0.0 , 0.0) "
				+ "dim 100.0 x 100.0 périmètre : 332.03 "
				+ "aire : 3673.66"
				+ " couleur = R0,G0,B0";
		
		assertEquals(expectedString,monEtoile.toString());
		
		Etoile monEtoile2 = new Etoile(new Coordonnees(15.89,56.62),39);
		monEtoile2.setRempli(true);
				
		String expectedString2 = "[Etoile-Rempli] : pos (15.89 , 56.62) "
				+ "dim 39.0 x 39.0 périmètre : 129.49 "
				+ "aire : 558.76"
				+ " couleur = R0,G0,B0";
		
		assertEquals(expectedString2,monEtoile2.toString());
	}

}
