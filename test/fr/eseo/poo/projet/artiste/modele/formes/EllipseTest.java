package fr.eseo.poo.projet.artiste.modele.formes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DecimalFormat;
import java.util.Locale;

import org.junit.Test;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;

/**
 * <h1> Test </h1>
 * <strong> Fonction <code>EllipseTest</code> </strong>
 * @author marindecacqueray
 * @see src.fr.eseo.projet.artiste.controleur.modele.forme.Ellipse
 */
public class EllipseTest {

	public static final double EPSILON = 0.001;
	
	double x1 = 10;
	double y1 = 19.54;
	double largeur1 = 23.5;
	double hauteur1 = 37.99;
	double x2 = 28.88;
	double y2 = -7;
	
	@Test
	public void constructeurTest1() {
		Ellipse monEllipse = new Ellipse();
		
		assertEquals(Forme.LARGEUR_PAR_DEFAUT, monEllipse.getLargeur(),EPSILON);
		assertEquals(Forme.HAUTEUR_PAR_DEFAUT, monEllipse.getHauteur(),EPSILON);
		assertEquals(new Coordonnees(), monEllipse.getPosition());
	}
	
	@Test
	public void constructeurTest2() {
		Ellipse monEllipse = new Ellipse(new Coordonnees(x1,y1));
		
		assertEquals(new Coordonnees(x1,y1),monEllipse.getPosition());
		assertEquals(Forme.LARGEUR_PAR_DEFAUT, monEllipse.getLargeur(),EPSILON);
		assertEquals(Forme.HAUTEUR_PAR_DEFAUT, monEllipse.getHauteur(),EPSILON);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructeurTest3Failed() throws Exception {
		new Ellipse(-1.22,22);
	}
	
	@Test
	public void constructeurTest3() {
		Ellipse monEllipse = new Ellipse(largeur1, hauteur1);
		
		assertEquals(new Coordonnees(), monEllipse.getPosition());
		assertEquals(largeur1, monEllipse.getLargeur(),EPSILON);
		assertEquals(hauteur1, monEllipse.getHauteur(),EPSILON);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructeurTest4Failed() throws Exception {
		new Ellipse(new Coordonnees(), -1.22, 22);
	}
	
	@Test
	public void constructeurTest4() {
		Ellipse monEllipse = new Ellipse(new Coordonnees(x1,y1), largeur1, hauteur1);
		
		assertEquals(new Coordonnees(x1,y1),monEllipse.getPosition());
		assertEquals(largeur1, monEllipse.getLargeur(),EPSILON);
		assertEquals(hauteur1, monEllipse.getHauteur(),EPSILON);
	}
	
	@Test
	public void toStringTest() {
		Ellipse monEllipse = new Ellipse();
		Locale currentLocale = Locale.getDefault();
		DecimalFormat current = (DecimalFormat)DecimalFormat.getNumberInstance(currentLocale);
		current.setMinimumFractionDigits(1);
		current.setMaximumFractionDigits(2);
		
		String expectedString = "[Ellipse] : pos (0.0 , 0.0) "
				+ "dim 100.0 x 100.0 périmètre : 314.16 "
				+ "aire : 7853.98"
				+ " couleur = R0,G0,B0";
		
		assertEquals(expectedString,monEllipse.toString());
		
		Ellipse monEllipse2 = new Ellipse(new Coordonnees(15.89,56.62),39.0,0.0);
				
		String expectedString2 = "[Ellipse] : pos (15.89 , 56.62) "
				+ "dim 39.0 x 0.0 périmètre : 77.97 "
				+ "aire : 0.0"
				+ " couleur = R0,G0,B0";
		
		assertEquals(expectedString2,monEllipse2.toString());
	}
	
	@Test
	public void aireTest() {
		Ellipse monEllipse = new Ellipse(largeur1, hauteur1);//23.5 37.99
		
		double a = Math.min(largeur1, hauteur1)/2;
		double b = Math.max(largeur1, hauteur1)/2;
		
		assertEquals(Math.PI*a*b, monEllipse.aire(),EPSILON);
		assertEquals(701.1759913, monEllipse.aire(),EPSILON);
		
	}
	
	@Test
	public void perimetreTest() {
		Ellipse monEllipse = new Ellipse(largeur1, hauteur1);//23.5 37.99
		
		double a = Math.min(largeur1, hauteur1)/2;
		double b = Math.max(largeur1, hauteur1)/2;
		double h = Math.pow((a-b)/(a+b),2);
		
		assertEquals(Math.PI*(a+b)*(1+(3*h /(10+Math.sqrt(4-3*h)))),monEllipse.perimetre(),EPSILON);
		assertEquals(97.93387284,monEllipse.perimetre(),EPSILON);
		 
	}
	
	@Test
	public void contientTest() {
		Ellipse monEllipse = new Ellipse(new Coordonnees(x1, y1), largeur1, hauteur1);//x1 = 10 y1 = 19.54 l = 23.5 h = 37.99
		Coordonnees p1 = new Coordonnees(-13.5, 45);
		Coordonnees p2 = new Coordonnees(11, 19.54);
		Coordonnees p3 = new Coordonnees(12,18);
		
		Ellipse monEllipse2 = new Ellipse();
		
		assertFalse(monEllipse.contient(p1));
		assertFalse(monEllipse.contient(p2));
		
		assertTrue(monEllipse2.contient(p3));
		assertTrue(monEllipse2.contient(p2));
	}
	
	@Test
	public void estRempliTest() {
		Ellipse monEllipse =  new Ellipse();
		
		assertFalse(monEllipse.estRempli());
		
		monEllipse.setRempli(true);
		
		assertTrue(monEllipse.estRempli());
	}
	

	@Test
	public void setLargeurTest() {
		Cercle monCercle = new Cercle();
		monCercle.setLargeur(largeur1);
		
		assertEquals(largeur1, monCercle.getLargeur(), EPSILON);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setLargeurTestFailed() throws Exception {
		Cercle monCercle = new Cercle();
		monCercle.setLargeur(-29);
	}
	
	@Test
	public void setHauteurTest() {
		Cercle monCercle = new Cercle();
		monCercle.setHauteur(hauteur1);
		
		assertEquals(hauteur1, monCercle.getHauteur(), EPSILON);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setHauteurTestFailed() throws Exception {
		Cercle monCercle = new Cercle();
		monCercle.setHauteur(-30);
	}
}
