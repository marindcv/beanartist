package fr.eseo.poo.projet.artiste.modele.formes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DecimalFormat;
import java.util.Locale;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;


import org.junit.Test;

public class LigneTest {
	
	public static final double EPSILON = 0.001;
	
	Coordonnees maPosition = new Coordonnees(10.200, 10.200);
	double x1 = 10;
	double y1 = 19.54;
	double largeur1 = 23.5;
	double hauteur1 = 37.99;
	double x2 = 28.88;
	double y2 = -7;
	double x3 = -100.100;
	double y3 = -100.100;
	
	@Test
	public void constructeurTest1() {
		Ligne maLigne = new Ligne();
		
		assertEquals(new Coordonnees(Coordonnees.ABSCISSE_PAR_DEFAUT, Coordonnees.ORDONNEE_PAR_DEFAUT),maLigne.getC1());
		assertEquals(Forme.LARGEUR_PAR_DEFAUT,maLigne.getLargeur(),EPSILON);
		assertEquals(Forme.HAUTEUR_PAR_DEFAUT,maLigne.getHauteur(),EPSILON);
	}
	
	@Test
	public void constructeurTest2() {
		Ligne maLigne = new Ligne(largeur1, hauteur1);
		
		assertEquals(new Coordonnees(Coordonnees.ABSCISSE_PAR_DEFAUT, Coordonnees.ORDONNEE_PAR_DEFAUT),maLigne.getC1());
		assertEquals(largeur1,maLigne.getLargeur(),EPSILON);
		assertEquals(hauteur1,maLigne.getHauteur(),EPSILON);
	}
	
	@Test
	public void constructeurTest3() {
		Ligne maLigne = new Ligne(new Coordonnees(x1,y1));
		
		assertEquals(new Coordonnees(x1, y1),maLigne.getC1());
		assertEquals(Forme.LARGEUR_PAR_DEFAUT,maLigne.getLargeur(),EPSILON);
		assertEquals(Forme.HAUTEUR_PAR_DEFAUT,maLigne.getHauteur(),EPSILON);
	}
	
	@Test
	public void constructeurTest4() {
		Ligne maLigne = new Ligne(new Coordonnees(x1,y1), largeur1, hauteur1);
			
		assertEquals(new Coordonnees(x1, y1),maLigne.getC1());
		assertEquals(largeur1,maLigne.getLargeur(),EPSILON);
		assertEquals(hauteur1,maLigne.getHauteur(),EPSILON);
	}
	
	@Test
	public void setC1Test() {
		Ligne maLigne = new Ligne();
		maLigne.setHauteur(hauteur1);//37.99
		maLigne.setLargeur(largeur1);//23.5
		maLigne.setC1(new Coordonnees(x1,y1));// x1 = 10 y1 = 19.54
		
		assertEquals(new Coordonnees(x1, y1),maLigne.getC1());
		assertEquals(new Coordonnees(x1, y1),maLigne.getPosition());
		assertEquals(maLigne.getC1().getOrdonnee() + maLigne.getHauteur(), maLigne.getC2().getOrdonnee(),EPSILON);
		assertEquals(maLigne.getC1().getAbscisse() + maLigne.getLargeur(), maLigne.getC2().getAbscisse(),EPSILON);
	}
	
	@Test
	public void setC2Test() {
		Ligne maLigne = new Ligne();//x = 10 y = 10
		maLigne.setC2(new Coordonnees(x1,y1));// x1 = 10 y1 = 19.54
		
		assertEquals(new Coordonnees(x1, y1),maLigne.getC2());
		assertEquals(maLigne.getC1().getOrdonnee() + maLigne.getHauteur(), maLigne.getC2().getOrdonnee(),EPSILON);
		assertEquals(maLigne.getC1().getAbscisse() + maLigne.getLargeur(), maLigne.getC2().getAbscisse(),EPSILON);
	}
	
	@Test
	public void aireTest() {
		Ligne maLigne = new Ligne();
		
		assertEquals(0, maLigne.aire(),EPSILON);
	}
	
	@Test
	public void perimetreTest() {
		Ligne maLigne = new Ligne(new Coordonnees(x1,y1), largeur1, hauteur1 );//x1 = 10 y1 = 19.54 largeur1 = 23.5 hauteur1 = 37.99
		
		assertEquals(maLigne.getC1().distanceVers(maLigne.getC2()),maLigne.perimetre(),EPSILON);//23.5
	}
	
	@Test
	public void toStringTest() {
		Locale currentLocale = Locale.getDefault();
		DecimalFormat current = (DecimalFormat)DecimalFormat.getNumberInstance(currentLocale);
		current.setMinimumFractionDigits(1);
		current.setMaximumFractionDigits(2);
		
		Ligne maLigne = new Ligne();
		maLigne.setC1(new Coordonnees(x1,y1));
		maLigne.setC2(new Coordonnees(x2,y2));
		
		Ligne maLigne2 = new Ligne();
		maLigne2.setC1(new Coordonnees(x2,y2));
		maLigne2.setC2(new Coordonnees(x1,y1));
		
		assertTrue(maLigne.getAngleVers() > 0);
		assertEquals("[Ligne] c1 : ("+x1+" , "+y1+") c2 : ("+x2+" , "+y2+") "
				+ "longueur : "+current.format(maLigne.getC1().distanceVers(maLigne.getC2()))+" "
				+ "angle : "+current.format(maLigne.getAngleVers())+"°"+" couleur = R0,G0,B0",maLigne.toString());
		
		assertTrue(maLigne2.getAngleVers() > 0);
		assertEquals("[Ligne] c1 : ("+x2+" , "+y2+") c2 : ("+x1+" , "+y1+") "
				+ "longueur : "+current.format(maLigne2.getC1().distanceVers(maLigne2.getC2()))+" "
				+ "angle : "+current.format(maLigne2.getAngleVers())+"°"+" couleur = R0,G0,B0",maLigne2.toString());
	}
	
	@Test
	public void setPositionTest() {
		Ligne maLigne = new Ligne();
		
		maLigne.setPosition(maPosition);
		
		assertEquals(maPosition,maLigne.getPosition());
	}
	
	@Test
	public void getCadreMaxXTest() {
		Ligne maLigne = new Ligne();//x = 10 y = 10
		maLigne.setC2(new Coordonnees(x2,y2));//x2 = 28.88 y2 = -7
		
		assertEquals(Math.max(	maLigne.getPosition().getAbscisse() + 
								maLigne.getLargeur(), 
								maLigne.getPosition().getAbscisse()),
								maLigne.getCadreMaxX(), EPSILON);
		
		Ligne maLigne2 = new Ligne(new Coordonnees(x3,y3));//x3 = -100.100 y3 = -100.100
		maLigne.setC2(new Coordonnees(x1, y1));//x1 = 10 y1 = 19.54
		
		assertEquals(Math.max(	maLigne2.getPosition().getAbscisse() + 
								maLigne2.getLargeur(), 
								maLigne2.getPosition().getAbscisse()),
								maLigne2.getCadreMaxX(), EPSILON);
	}
	
	@Test
	public void getCadreMinXTest() {
		Ligne maLigne = new Ligne();//x = 10 y = 10
		maLigne.setC2(new Coordonnees(x2,y2));//x2 = 28.88 y2 = -7
		
		assertEquals(Math.min(	maLigne.getPosition().getAbscisse() + 
								maLigne.getLargeur(), 
								maLigne.getPosition().getAbscisse()),
								maLigne.getCadreMinX(), EPSILON);
		
		Ligne maLigne2 = new Ligne(new Coordonnees(x3,y3));//x3 = -100.100 y3 = -100.100
		maLigne.setC2(new Coordonnees(x3, y3));
		
		assertEquals(Math.min(	maLigne2.getPosition().getAbscisse() + 
								maLigne2.getLargeur(), 
								maLigne2.getPosition().getAbscisse()),
								maLigne2.getCadreMinX(), EPSILON);
	}
	
	@Test
	public void getCadreMaxYTest() {
		Ligne maLigne = new Ligne();//x = 10 y = 10
		maLigne.setC2(new Coordonnees(x2,y2));//x2 = 28.88 y2 = -7
		
		assertEquals(Math.max(	maLigne.getPosition().getOrdonnee() + 
								maLigne.getHauteur(), 
								maLigne.getPosition().getOrdonnee()),
								maLigne.getCadreMaxY(), EPSILON);
		
		Ligne maLigne2 = new Ligne(new Coordonnees(x3,y3));//x3 = -100.100 y3 = -100.100
		maLigne.setC2(new Coordonnees(x3, y3));
		
		assertEquals(Math.max(	maLigne2.getPosition().getOrdonnee() + 
								maLigne2.getHauteur(), 
								maLigne2.getPosition().getOrdonnee()),
								maLigne2.getCadreMaxY(), EPSILON);
	}
	
	@Test
	public void getCadreMinYTest() {
		Ligne maLigne = new Ligne();//x = 10 y = 10
		maLigne.setC2(new Coordonnees(x2,y2));//x2 = 28.88 y2 = -7
		
		assertEquals(Math.min(	maLigne.getPosition().getOrdonnee() + 
								maLigne.getHauteur(), 
								maLigne.getPosition().getOrdonnee()),
								maLigne.getCadreMinY(), EPSILON);
		
		Ligne maLigne2 = new Ligne(new Coordonnees(x3,y3));//x3 = -100.100 y3 = -100.100
		maLigne.setC2(new Coordonnees(x3, y3));
		
		assertEquals(Math.min(	maLigne2.getPosition().getOrdonnee() + 
								maLigne2.getHauteur(), 
								maLigne2.getPosition().getOrdonnee()),
								maLigne2.getCadreMinY(), EPSILON);
	}
	
	@Test
	public void contient() {
		Ligne maLigne = new Ligne();//x = 10 y = 10
		maLigne.setC2(new Coordonnees(20,20));
		Coordonnees p1 = new Coordonnees(28,67);
		Coordonnees p2 = new Coordonnees(15,15.4);
		
		assertFalse(maLigne.contient(p1));
		assertTrue(maLigne.contient(p2));
		
	}

}
